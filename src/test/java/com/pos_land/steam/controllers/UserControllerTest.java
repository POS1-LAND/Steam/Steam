package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.UserDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest {

    private UserController userController;

    @Mock
    private UserService userService;

    @Mock
    private GameService gameService;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        userController = new UserController(userService, gameService);

        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void testGetUserById() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);

        when(userService.getById(anyLong())).thenReturn(userDTO);

        mockMvc.perform(get("/user/1/view"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/view"))
                .andExpect(model().attributeExists("user"));

        verify(userService, times(1)).getById(anyLong());
    }

    @Test
    public void testGetUsers() throws Exception {
        Set<UserDTO> userDTOS = new HashSet<>();
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTOS.add(userDTO);

        when(userService.getSortedUsers()).thenReturn(userDTOS);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(view().name("user"))
                .andExpect(model().attributeExists("users"));

        verify(userService, times(1)).getSortedUsers();
        verify(userService, never()).getUsers();
        verify(userService, never()).getById(anyLong());
    }

    @Test
    public void testCreateUserView() throws Exception {
        mockMvc.perform(get("/user/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/create"))
                .andExpect(model().attributeExists("user"));
    }

    @Test
    public void testDeleteUserById() throws Exception {
        mockMvc.perform(get("/user/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/users"));

        verify(userService, times(1)).deleteById(anyLong());
    }

    @Test
    public void updateUserView() throws Exception {

        when(userService.getById(anyLong())).thenReturn(new UserDTO());

        mockMvc.perform(get("/user/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/update"))
                .andExpect(model().attributeExists("user"));

        verify(userService, times(1)).getById(anyLong());
        verify(userService, never()).getUsers();
        verify(userService, never()).getSortedUsers();
    }

    @Test
    public void testUpdateUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);

        when(userService.getById(anyLong())).thenReturn(userDTO);
        when(userService.saveUser(any())).thenReturn(userDTO);

        mockMvc.perform(post("/user/update/submit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("userName", "some name")
                .param("password", "some pw")
                .param("email", "some email"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user/1/view"));

        verify(userService, times(1)).saveUser(any());
        verify(userService, times(1)).getById(any());
    }

    @Test
    public void testCreateUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);

        when(userService.saveUser(any())).thenReturn(userDTO);

        mockMvc.perform(post("/user/create/submit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("userName", "some name")
                .param("password", "some pw")
                .param("email", "some email"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user/1/view"));

        verify(userService, times(1)).saveUser(any());
    }

    @Test
    public void testGetUserNotFound() throws Exception {

        when(userService.getById(anyLong())).thenThrow(NotFoundException.class);

        mockMvc.perform(get("/user/1/view"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("404error"));

        verify(userService, times(1)).getById(anyLong());
        verify(userService, never()).getUsers();
        verify(userService, never()).getSortedUsers();
    }

    @Test
    public void testAddGameToUser() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);
        when(userService.getById(anyLong())).thenReturn(userDTO);

        mockMvc.perform(get("/user/1/game/1/add"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user/1/update"));

        assertEquals(1, userDTO.getGames().size());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
        verify(userService, times(1)).getById(anyLong());
        verify(userService, never()).getUsers();
        verify(userService, never()).getSortedUsers();
    }

    @Test
    public void testDeleteGameFromUser() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);

        userDTO.getGames().add(gameDTO);

        when(userService.getById(anyLong())).thenReturn(userDTO);

        mockMvc.perform(get("/user/1/game/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user/1/update"));

        assertEquals(0, userDTO.getGames().size());

        verify(userService, times(1)).getById(anyLong());
        verify(userService, never()).getUsers();
        verify(userService, never()).getSortedUsers();
    }
}
package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.services.DeveloperService;
import com.pos_land.steam.services.GameService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeveloperControllerTest {

    private DeveloperController developerController;

    @Mock
    private DeveloperService developerService;

    @Mock
    private GameService gameService;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        developerController = new DeveloperController(developerService, gameService);

        mockMvc = MockMvcBuilders.standaloneSetup(developerController).setControllerAdvice(new ControllerExceptionHandler()).build();

    }

    @Test
    public void getDevelopersTest() throws Exception {
        SortedSet<DeveloperDTO> developerDTOSet = new TreeSet<>();
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);
        developerDTO.setDeveloperName("DeveloperName");

        developerDTOSet.add(developerDTO);

        when(developerService.getSortedDevelopers()).thenReturn(developerDTOSet);
        mockMvc.perform(get("/developers"))
                .andExpect(status().isOk())
                .andExpect(view().name("developer"))
                .andExpect(model().attributeExists("developers"));
        verify(developerService, times(1)).getSortedDevelopers();
    }

    @Test
    public void getDeveloperByIdTest() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setDeveloperName("DeveloperName");
        developerDTO.setId(1L);

        when(developerService.getById(anyLong())).thenReturn(developerDTO);

        mockMvc.perform(get("/developer/1/view"))
                .andExpect(status().isOk())
                .andExpect(view().name("developer/view"))
                .andExpect(model().attributeExists("developer"));
        verify(developerService, times(1)).getById(anyLong());

        verify(developerService, never()).getDevelopers();
        verify(developerService, never()).getSortedDevelopers();
    }

    @Test
    public void createDeveloperTest() throws Exception {

        mockMvc.perform(get("/developer/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("developer/create"))
                .andExpect(model().attributeExists("developer"));
    }

    @Test
    public void createTest() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);

        when(developerService.saveDeveloper(any())).thenReturn(developerDTO);

        mockMvc.perform(post("/developer/create/submit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("developerName", "some name"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/developer/1/view"));

        verify(developerService, times(1)).saveDeveloper(any());
    }

    @Test
    public void updateDeveloperTest() throws Exception {

        when(developerService.getById(anyLong())).thenReturn(new DeveloperDTO());

        mockMvc.perform(get("/developer/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("developer/update"))
                .andExpect(model().attributeExists("developer"));

        verify(developerService, times(1)).getById(anyLong());
    }

    @Test
    public void updateTest() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);

        when(developerService.saveDeveloper(any())).thenReturn(developerDTO);

        mockMvc.perform(post("/developer/update/submit")
                .param("id", "1")
                .param("developerName", "some name"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/developer/1/view"));

        verify(developerService, times(1)).saveDeveloper(any());
    }

    @Test
    public void deleteDeveloperTest() throws Exception {
        mockMvc.perform(get("/developer/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/developers"));

        verify(developerService, times(1)).deleteById(anyLong());

    }

    @Test
    public void testGetDeveloperNotFound() throws Exception {

        when(developerService.getById(anyLong())).thenThrow(NotFoundException.class);

        mockMvc.perform(get("/developer/1/view"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("404error"));

        verify(developerService, times(1)).getById(anyLong());
    }

    @Test
    public void testAddGameToDeveloper() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);
        when(developerService.getById(anyLong())).thenReturn(developerDTO);

        mockMvc.perform(get("/developer/1/game/1/add"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/developer/1/update"));

        assertEquals(1, gameDTO.getDevelopers().size());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(developerService, times(1)).getById(anyLong());
    }

    @Test
    public void testDeleteGameFromDeveloper() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);

        gameDTO.getDevelopers().add(developerDTO);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);
        when(developerService.getById(anyLong())).thenReturn(developerDTO);

        mockMvc.perform(get("/developer/1/game/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/developer/1/update"));

        assertEquals(0, gameDTO.getDevelopers().size());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(developerService, times(1)).getById(anyLong());
    }
}
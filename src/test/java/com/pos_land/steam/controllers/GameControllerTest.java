package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.services.DeveloperService;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.LanguageService;
import com.pos_land.steam.services.PublisherService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GameControllerTest {

    private static final String GAME_NAME = "Testgame";
    private static final Long GAME_ID = 1L;

    @Mock
    private GameService gameService;

    @Mock
    private PublisherService publisherService;

    @Mock
    private DeveloperService developerService;

    @Mock
    private LanguageService languageService;

    private GameController gameController;
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        gameController = new GameController(gameService, publisherService, developerService, languageService);
        mockMvc = MockMvcBuilders.standaloneSetup(gameController).setControllerAdvice(new ControllerExceptionHandler()).build();
    }

    @Test
    public void getGamesTest() throws Exception {
        Set<GameDTO> gameSet = new HashSet<>();

        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setGameName(GAME_NAME);
        gameSet.add(gameDTO);

        when(gameService.getSortedGames()).thenReturn(gameSet);

        mockMvc.perform(get("/games"))
                .andExpect(status().isOk())
                .andExpect(view().name("game"))
                .andExpect(model().attributeExists("games"));

        verify(gameService, times(1)).getSortedGames();
    }

    @Test
    public void getGameByIdTest() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setGameName(GAME_NAME);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);

        mockMvc.perform(get("/game/1/view"))
                .andExpect(status().isOk())
                .andExpect(view().name("game/view"))
                .andExpect(model().attributeExists("game"));

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
    }

    @Test
    public void createGameTest() throws Exception {

        mockMvc.perform(get("/game/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("game/create"))
                .andExpect(model().attributeExists("game"))
                .andExpect(model().attributeExists("allPublishers"))
                .andExpect(model().attributeExists("allGenres"));

        verify(publisherService, times(1)).getSortedPublishers();
    }

    @Test
    public void createTest() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setGameName(GAME_NAME);

        when(gameService.saveGame(any())).thenReturn(gameDTO);

        mockMvc.perform(post("/game/create/submit")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("id", "1")
                .param("gameName", "Testgame"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/game/1/view"))
                .andExpect(model().attributeExists("game"));

        verify(gameService, times(1)).saveGame(any());
    }

    @Test
    public void deleteGameTest() throws Exception {
        mockMvc.perform(get("/game/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/games"));

        verify(gameService, times(1)).deleteById(anyLong());
    }

    @Test
    public void updateGameTest() throws Exception {
        when(gameService.getById(anyLong())).thenReturn(new GameDTO());

        mockMvc.perform(get("/game/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("game/update"))
                .andExpect(model().attributeExists("game"))
                .andExpect(model().attributeExists("allGenres"))
                .andExpect(model().attributeExists("allPublishers"))
                .andExpect(model().attributeExists("allDevelopers"))
                .andExpect(model().attributeExists("allLanguages"));

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();

        verify(languageService, times(1)).getDisplayedLanguages(any());
        verify(languageService, never()).getLanguages();
        verify(languageService, never()).getSortedLanguages();
        verify(languageService, never()).getById(anyLong());

        verify(publisherService, times(1)).getSortedPublishers();
        verify(publisherService, never()).getPublishers();
        verify(publisherService, never()).getById(anyLong());
    }

    @Test
    public void updateTest() throws Exception {

        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setGameName(GAME_NAME);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);
        when(gameService.saveGame(any())).thenReturn(gameDTO);

        mockMvc.perform(post("/game/update/submit")
                .param("id", "1")
                .param("gameName", "Testgame"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/game/1/view"));

        verify(gameService, times(1)).saveGame(any());
        verify(gameService, times(1)).getById(any());
    }

    @Test
    public void testAddDeveloperToGame() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);

        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        when(developerService.getById(anyLong())).thenReturn(developerDTO);
        when(gameService.getById(anyLong())).thenReturn(gameDTO);

        mockMvc.perform(get("/game/1/developer/1/add"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/game/1/update"));

        assertEquals(1, gameDTO.getDevelopers().size());

        verify(developerService, times(1)).getById(anyLong());
        verify(developerService, never()).getDevelopers();
        verify(developerService, never()).getSortedDevelopers();
        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
    }

    @Test
    public void testDeleteDeveloperFromGame() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);

        gameDTO.getDevelopers().add(developerDTO);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);

        mockMvc.perform(get("/game/1/developer/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/game/1/update"));

        assertEquals(0, gameDTO.getDevelopers().size());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
    }

    @Test
    public void testAddLanguageToGame() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        LanguageDTO languageDTO = new LanguageDTO();
        languageDTO.setId(1L);

        when(languageService.getById(anyLong())).thenReturn(languageDTO);
        when(gameService.getById(anyLong())).thenReturn(gameDTO);

        mockMvc.perform(get("/game/1/language/1/add"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/game/1/update"));

        assertEquals(1, gameDTO.getLanguages().size());

        verify(gameService, times(1)).saveGame(any());
        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
        verify(languageService, times(1)).getById(anyLong());
        verify(languageService, never()).getLanguages();
        verify(languageService, never()).getSortedLanguages();
    }

    @Test
    public void testDeleteLanguageFromGame() throws Exception {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);

        LanguageDTO languageDTO = new LanguageDTO();
        languageDTO.setId(1L);

        gameDTO.getLanguages().add(languageDTO);

        when(gameService.getById(anyLong())).thenReturn(gameDTO);

        assertEquals(1, gameDTO.getLanguages().size());

        mockMvc.perform(get("/game/1/language/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/game/1/update"));

        assertEquals(0, gameDTO.getLanguages().size());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, times(1)).saveGame(any());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
    }
}
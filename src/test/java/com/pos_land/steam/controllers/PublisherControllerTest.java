package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.PublisherDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.PublisherService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PublisherControllerTest {

    private PublisherController publisherController;

    @Mock
    private PublisherService publisherService;

    @Mock
    private GameService gameService;

    @Mock
    private Model model;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        publisherController = new PublisherController(publisherService, gameService);

        mockMvc = MockMvcBuilders.standaloneSetup(publisherController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
    }

    @Test
    public void getPublishersTest() throws Exception {
        Set<PublisherDTO> publisherDTOSet = new HashSet<>();
        publisherDTOSet.add(new PublisherDTO());
        publisherDTOSet.add(new PublisherDTO());

        when(publisherService.getSortedPublishers()).thenReturn(publisherDTOSet);

        mockMvc.perform(get("/publishers"))
                .andExpect(status().isOk())
                .andExpect(view().name("publisher"))
                .andExpect(model().attributeExists("publishers"));

        verify(publisherService, times(1)).getSortedPublishers();
        verify(publisherService, never()).getPublishers();
        verify(publisherService, never()).getById(anyLong());
    }

    @Test
    public void getPublisherByIdTest() throws Exception {
        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(1L);

        when(publisherService.getById(anyLong())).thenReturn(publisherDTO);

        mockMvc.perform(get("/publisher/1/view"))
                .andExpect(status().isOk())
                .andExpect(view().name("publisher/view"))
                .andExpect(model().attributeExists("publisher"));

        verify(publisherService, times(1)).getById(anyLong());
        verify(publisherService, never()).getPublishers();
        verify(publisherService, never()).getSortedPublishers();
    }

    @Test
    public void createPublisher() throws Exception {

        mockMvc.perform(get("/publisher/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("publisher/create"))
                .andExpect(model().attributeExists("publisher"));
    }

    @Test
    public void deletePublisher() throws Exception {
        mockMvc.perform(get("/publisher/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/publishers"));

        verify(publisherService, times(1)).deleteById(anyLong());
    }

    @Test
    public void updatePublisher() throws Exception {
        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(1L);

        when(publisherService.getById(anyLong())).thenReturn(publisherDTO);

        mockMvc.perform(get("/publisher/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("publisher/update"))
                .andExpect(model().attributeExists("publisher"))
                .andExpect(model().attributeExists("allGames"));

        verify(publisherService, times(1)).getById(anyLong());
        verify(publisherService, never()).getPublishers();
        verify(publisherService, never()).getSortedPublishers();
    }


    @Test
    public void testGetPublisherNotFound() throws Exception {

        when(publisherService.getById(anyLong())).thenThrow(NotFoundException.class);

        mockMvc.perform(get("/publisher/1/view"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("404error"));

        verify(publisherService, times(1)).getById(anyLong());
        verify(publisherService, never()).getPublishers();
        verify(publisherService, never()).getSortedPublishers();
    }

    @Test
    public void testDeleteGame() throws Exception {
        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(1L);

        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);
        gameDTO.setPublisher(publisherDTO);

        assertNotNull(gameDTO.getPublisher());

        when(gameService.getById(anyLong())).thenReturn(gameDTO);

        mockMvc.perform(get("/publisher/1/game/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/publisher/1/update"));

        assertNull(gameDTO.getPublisher());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
    }

    @Test
    public void testAddGame() throws Exception {
        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(1L);

        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(1L);
        gameDTO.setPublisher(null);

        assertNull(gameDTO.getPublisher());

        when(gameService.getById(anyLong())).thenReturn(gameDTO);
        when(publisherService.getById(anyLong())).thenReturn(publisherDTO);

        mockMvc.perform(get("/publisher/1/game/1/add"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/publisher/1/update"));

        assertNotNull(gameDTO.getPublisher());

        verify(gameService, times(1)).getById(anyLong());
        verify(gameService, never()).getGames();
        verify(gameService, never()).getSortedGames();
    }
}
package com.pos_land.steam.services;

import com.pos_land.steam.converters.UserConverter;
import com.pos_land.steam.domain.User;
import com.pos_land.steam.dto.UserDTO;
import com.pos_land.steam.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplIT {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConverter userConverter;

    public static final String NEW_USER_NAME = "This is a totally original new username hey look at meeeeeee";

    @Transactional
    @Test
    public void testSaveUser() {
        Iterable<User> users = userRepository.findAll();
        User testUser = users.iterator().next();
        UserDTO testUserDTO = userConverter.convert(testUser);

        testUserDTO.setUserName(NEW_USER_NAME);
        UserDTO savedUserDTO = userService.saveUser(testUserDTO);

        assertEquals(NEW_USER_NAME, savedUserDTO.getUserName());
        assertEquals(testUserDTO.getGames().size(), savedUserDTO.getGames().size());
        assertEquals(testUserDTO.getId(), savedUserDTO.getId());
        assertEquals(testUserDTO.getEmail(), savedUserDTO.getEmail());
        assertEquals(testUserDTO.getPassword(), savedUserDTO.getPassword());
    }
}

package com.pos_land.steam.services;

import com.pos_land.steam.converters.LanguageConverter;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.repositories.LanguageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LanguageServiceImplIT {

    @Autowired
    LanguageService languageService;

    @Autowired
    LanguageRepository languageRepository;

    @Autowired
    LanguageConverter languageConverter;

    @Test
    @Transactional
    public void getDisplayLanguagesTest() {
        Set<LanguageDTO> allLanguages = languageService.getLanguages();

        Set<LanguageDTO> myLanguages = new HashSet<>();
        allLanguages.forEach(gameDTO -> {
            if (gameDTO.getId().longValue() == 1) {
                myLanguages.add(gameDTO);
            }
        });

        Set<LanguageDTO> returnedSet = languageService.getDisplayedLanguages(myLanguages);

        assertNotEquals(returnedSet.size(), allLanguages.size());
        assertEquals(allLanguages.size() - 1, returnedSet.size());
    }
}

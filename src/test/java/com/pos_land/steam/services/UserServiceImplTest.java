package com.pos_land.steam.services;

import com.pos_land.steam.converters.*;
import com.pos_land.steam.domain.User;
import com.pos_land.steam.dto.UserDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {

    private UserService userService;

    // @Mock because UserRepository is an Interface
    @Mock
    private UserRepository userRepository;

    private UserConverter userConverter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        userConverter = new UserConverter(new GameConverter(new SimpleDeveloperConverter(), new SimplePublisherConverter(), new LanguageConverter()));
        userService = new UserServiceImpl(userRepository, userConverter);
    }

    @Test
    public void testDeleteById() throws Exception {
        Long idToDelete = Long.valueOf(2);

        userService.deleteById(idToDelete);

        verify(userRepository, times(1)).deleteById(anyLong());
    }

    @Test
    public void getUsersTest() throws Exception {

        User user = new User();
        Set<User> userSet = new HashSet<>();
        userSet.add(user);

        when(userRepository.findAll()).thenReturn(userSet);

        Set<UserDTO> users = userService.getUsers();

        assertEquals(1, users.size());
        verify(userRepository, times(1)).findAll();
        verify(userRepository, never()).findById(anyLong());
    }

    @Test
    public void getUserByIdTest() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setUserName("hi");
        Optional<User> userOptional = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userOptional);

        UserDTO userDTO = userService.getById(1L);

        assertNotNull("Null UserDTO returned", userDTO);
        verify(userRepository, times(1)).findById(anyLong());
        verify(userRepository, never()).findAll();
    }

    @Test(expected = NotFoundException.class)
    public void getUserByIdTestNotFound() throws Exception {
        Optional<User> userOptional = Optional.empty();

        when(userRepository.findById(anyLong())).thenReturn(userOptional);

        userService.getById(1L);
    }

    @Test
    public void getSortedUsersTest() {
        User user = new User();
        user.setId(1L);
        Set<User> userSet = new HashSet<>();
        userSet.add(user);

        when(userRepository.findAll()).thenReturn(userSet);

        Set<UserDTO> users = userService.getSortedUsers();

        assertEquals(1, users.size());
        verify(userRepository, times(1)).findAll();
        verify(userRepository, never()).findById(anyLong());
    }
}
package com.pos_land.steam.services;

import com.pos_land.steam.converters.PublisherConverter;
import com.pos_land.steam.converters.SimpleGameConverter;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.PublisherDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.PublisherRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class PublisherServiceImplTest {

    @Mock
    private PublisherRepository publisherRepository;
    private PublisherConverter publisherConverter;
    private PublisherService publisherService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        publisherConverter = new PublisherConverter(new SimpleGameConverter());
        publisherService = new PublisherServiceImpl(publisherRepository, publisherConverter);
    }

    @Test
    public void getPublishers() throws Exception {
        Publisher publisher = new Publisher();
        Set<Publisher> publisherSet = new HashSet<>();
        publisherSet.add(publisher);

        when(publisherRepository.findAll()).thenReturn(publisherSet);

        Set<PublisherDTO> publisherDTOSet = publisherService.getPublishers();

        assertEquals(1, publisherDTOSet.size());
        verify(publisherRepository, times(1)).findAll();
        verify(publisherRepository, never()).findById(anyLong());
    }

    @Test
    public void getById() throws Exception {
        Publisher publisher = new Publisher();
        publisher.setId(1L);
        Optional<Publisher> publisherOptional = Optional.of(publisher);

        when(publisherRepository.findById(anyLong())).thenReturn(publisherOptional);

        PublisherDTO publisherDTO = publisherService.getById(1L);

        assertNotNull(publisherDTO);
        verify(publisherRepository, times(1)).findById(anyLong());
        verify(publisherRepository, never()).findAll();
    }

    @Test
    public void savePublisher() throws Exception {

        Publisher publisher = new Publisher();
        publisher.setId(1L);

        PublisherDTO publisherDTO = publisherConverter.convert(publisher);

        when(publisherRepository.save(any())).thenReturn(publisher);

        PublisherDTO savedPublisher = publisherService.savePublisher(publisherDTO);

        assertNotNull(savedPublisher);
        verify(publisherRepository, times(1)).save(any());
    }

    @Test
    public void deleteById() throws Exception {
        Long id = 1L;

        publisherService.deleteById(id);

        verify(publisherRepository, times(1)).deleteById(anyLong());
        verify(publisherRepository, never()).deleteAll();
    }

    @Test(expected = NotFoundException.class)
    public void getPublisherByIdTestNotFound() throws Exception {
        Optional<Publisher> publisherOptional = Optional.empty();

        when(publisherRepository.findById(anyLong())).thenReturn(publisherOptional);

        publisherService.getById(1L);
    }

    @Test
    public void getSortedPublishersTest() {
        Publisher publisher = new Publisher();
        publisher.setId(1L);
        Set<Publisher> publisherSet = new HashSet<>();
        publisherSet.add(publisher);

        when(publisherRepository.findAll()).thenReturn(publisherSet);

        Set<PublisherDTO> publisherDTOSet = publisherService.getSortedPublishers();

        assertEquals(1, publisherDTOSet.size());
        verify(publisherRepository, times(1)).findAll();
        verify(publisherRepository, never()).findById(anyLong());
    }
}
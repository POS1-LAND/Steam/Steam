package com.pos_land.steam.services;

import com.pos_land.steam.converters.DeveloperConverter;
import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.repositories.DeveloperRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DeveloperServiceImplIT {

    private static final String DEV_NAME = "DeveloperName";

    @Autowired
    private DeveloperService developerService;

    @Autowired
    private DeveloperConverter developerConverter;

    @Autowired
    private DeveloperRepository developerRepository;

    @Test
    @Transactional
    public void saveDeveloperTest() throws Exception {
        Iterable<Developer> developers = developerRepository.findAll();
        Developer testDeveloper = developers.iterator().next();
        DeveloperDTO testDeveloperDTO = developerConverter.convert(testDeveloper);

        testDeveloperDTO.setDeveloperName(DEV_NAME);

        DeveloperDTO savedDeveloper = developerService.saveDeveloper(testDeveloperDTO);

        assertEquals(DEV_NAME, savedDeveloper.getDeveloperName());

    }

    @Test
    @Transactional
    public void testGetSelectedDevelopers() {
        Set<DeveloperDTO> allDevelopers = developerService.getDevelopers();

        Set<DeveloperDTO> myDevelopers = new HashSet<>();
        allDevelopers.forEach(developerDTO -> {
            if (developerDTO.getId().longValue() == 1) {
                myDevelopers.add(developerDTO);
            }
        });

        Set<DeveloperDTO> returnedSet = developerService.getSelectedDevelopers(myDevelopers);

        assertNotEquals(returnedSet.size(), allDevelopers.size());
        assertEquals(allDevelopers.size() - 1, returnedSet.size());

    }

}
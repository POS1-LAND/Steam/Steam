package com.pos_land.steam.services;

import com.pos_land.steam.converters.PublisherConverter;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.PublisherDTO;
import com.pos_land.steam.repositories.PublisherRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PublisherServiceImplIT {

    @Autowired
    PublisherService publisherService;

    @Autowired
    PublisherRepository publisherRepository;

    @Autowired
    PublisherConverter publisherConverter;

    @Test
    @Transactional
    public void savePublisherTest() {
        Iterable<Publisher> publishers = publisherRepository.findAll();
        Publisher testPublisher = publishers.iterator().next();
        PublisherDTO testPublisherDTO = publisherConverter.convert(testPublisher);

        testPublisherDTO.setId(1L);
        testPublisherDTO.setPublisherName("some name");

        PublisherDTO savedPublisher = publisherService.savePublisher(testPublisherDTO);

        assertEquals("some name", savedPublisher.getPublisherName());
        assertEquals(1L, savedPublisher.getId().longValue());
    }
}

package com.pos_land.steam.services;

import com.pos_land.steam.converters.GameConverter;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.repositories.GameRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceImplIT {

    GameService gameService;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    GameConverter gameConverter;

    @Before
    public void setUp() throws Exception {
        gameService = new GameServiceImpl(gameRepository, gameConverter);
    }

    @Test
    @Transactional
    public void saveGameTest() {
        Iterable<Game> games = gameRepository.findAll();
        Game testGame = games.iterator().next();
        GameDTO testGameDTO = gameConverter.convert(testGame);

        testGameDTO.setId(1L);
        testGameDTO.setGameName("some name");

        GameDTO savedGame = gameService.saveGame(testGameDTO);

        assertEquals("some name", savedGame.getGameName());
        assertEquals(1L, savedGame.getId().longValue());
    }

    @Test
    @Transactional
    public void testGetDisplayedGames() {

        Set<GameDTO> allGames = gameService.getGames();

        Set<GameDTO> myGames = new HashSet<>();
        allGames.forEach(gameDTO -> {
            if (gameDTO.getId().longValue() == 1) {
                myGames.add(gameDTO);
            }
        });

        Set<GameDTO> returnedSet = gameService.getDisplayedGames(myGames);

        assertNotEquals(returnedSet.size(), allGames.size());
        assertEquals(allGames.size() - 1, returnedSet.size());

    }

}

package com.pos_land.steam.services;

import com.pos_land.steam.converters.GameConverter;
import com.pos_land.steam.converters.LanguageConverter;
import com.pos_land.steam.converters.SimpleDeveloperConverter;
import com.pos_land.steam.converters.SimplePublisherConverter;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.GameRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class GameServiceImplTest {

    private GameService gameService;

    // @Mock because GameRepository is an Interface
    @Mock
    private GameRepository gameRepository;

    private GameConverter gameConverter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        gameConverter = new GameConverter(new SimpleDeveloperConverter(), new SimplePublisherConverter(), new LanguageConverter());

        gameService = new GameServiceImpl(gameRepository, gameConverter);
    }

    @Test
    public void testDeleteById() throws Exception {
        Long idToDelete = Long.valueOf(2);

        gameService.deleteById(idToDelete);

        verify(gameRepository, times(1)).deleteById(anyLong());
    }

    @Test
    public void getGamesTest() throws Exception {
        Game game = new Game();
        Set<Game> gameSet = new HashSet<>();
        gameSet.add(game);

        when(gameRepository.findAll()).thenReturn(gameSet);

        Set<GameDTO> games = gameService.getGames();

        assertEquals(1, games.size());
        verify(gameRepository, times(1)).findAll();
        verify(gameRepository, never()).findById(anyLong());
    }

    @Test(expected = NotFoundException.class)
    public void getGameByIdTestNotFound() throws Exception {
        Optional<Game> gameOptional = Optional.empty();

        when(gameRepository.findById(anyLong())).thenReturn(gameOptional);

        gameService.getById(1L);
    }

    @Test
    public void getGameByIdTest() {
        Game game = new Game();
        game.setId(1L);

        Optional<Game> gameOptional = Optional.of(game);

        when(gameRepository.findById(anyLong())).thenReturn(gameOptional);

        GameDTO gameDTO = gameService.getById(1L);

        assertNotNull("GameDTO not found.", gameDTO);
        verify(gameRepository, times(1)).findById(anyLong());
        verify(gameRepository, never()).findAll();
    }

    @Test
    public void getSortedGamesTest() {
        Game game = new Game();
        game.setId(1L);
        SortedSet<Game> gameSet = new TreeSet<>(Comparator.comparing(Game::getId));
        gameSet.add(game);

        when(gameRepository.findAll()).thenReturn(gameSet);

        Set<GameDTO> games = gameService.getSortedGames();

        assertEquals(1, games.size());
        verify(gameRepository, times(1)).findAll();
        verify(gameRepository, never()).findById(anyLong());
    }
}
package com.pos_land.steam.services;

import com.pos_land.steam.converters.LanguageConverter;
import com.pos_land.steam.domain.Language;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.LanguageRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class LanguageServiceImplTest {

    LanguageService languageService;

    @Mock
    LanguageRepository languageRepository;

    LanguageConverter languageConverter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        languageConverter = new LanguageConverter();
        languageService = new LanguageServiceImpl(languageRepository, languageConverter);
    }

    @Test
    public void getLanguagesTest() {
        Language language = new Language();
        Set<Language> languages = new HashSet<>();
        languages.add(language);

        when(languageRepository.findAll()).thenReturn(languages);

        Set<LanguageDTO> languageDTOSet = languageService.getLanguages();

        assertEquals(languages.size(), languageDTOSet.size());
        verify(languageRepository, times(1)).findAll();
        verify(languageRepository, never()).findById(anyLong());
    }

    @Test
    public void getLanguageByIdTest() {
        Language language = new Language();
        language.setId(1L);

        Optional<Language> languageOptional = Optional.of(language);

        when(languageRepository.findById(anyLong())).thenReturn(languageOptional);

        LanguageDTO languageDTO = languageService.getById(1L);

        assertNotNull("LanguageDTO not found.", languageDTO);
        verify(languageRepository, times(1)).findById(anyLong());
        verify(languageRepository, never()).findAll();
    }

    @Test
    public void getSortedLanguagesTest() {
        Language language = new Language();
        language.setId(1L);
        Set<Language> languages = new HashSet<>();
        languages.add(language);

        when(languageRepository.findAll()).thenReturn(languages);

        Set<LanguageDTO> languageDTOSet = languageService.getSortedLanguages();

        assertEquals(languages.size(), languageDTOSet.size());
        verify(languageRepository, times(1)).findAll();
        verify(languageRepository, never()).findById(anyLong());
    }

    @Test(expected = NotFoundException.class)
    public void getLanguageByIdNotFoundTest() {
        Optional<Language> languageOptional = Optional.empty();

        when(languageRepository.findById(anyLong())).thenReturn(languageOptional);

        languageService.getById(1L);
    }
}
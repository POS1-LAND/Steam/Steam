package com.pos_land.steam.services;

import com.pos_land.steam.converters.DeveloperConverter;
import com.pos_land.steam.converters.SimpleGameConverter;
import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.DeveloperRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class DeveloperServiceImplTest {

    private DeveloperService developerService;

    @Mock
    private DeveloperRepository developerRepository;

    private DeveloperConverter developerConverter;

    private SimpleGameConverter simpleGameConverter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        simpleGameConverter = new SimpleGameConverter();
        developerConverter = new DeveloperConverter(simpleGameConverter);
        developerService = new DeveloperServiceImpl(developerRepository, developerConverter);
    }

    @Test
    public void saveDeveloperTest() {
        Developer developer = new Developer();
        developer.setId(1L);

        DeveloperDTO developerDTO = developerConverter.convert(developer);

        when(developerRepository.save(any())).thenReturn(developer);

        DeveloperDTO savedDeveloper = developerService.saveDeveloper(developerDTO);

        assertNotNull("Developer not found", savedDeveloper);
        verify(developerRepository, times(1)).save(any());

    }

    @Test
    public void getDevelopersTest() throws Exception {
        Developer developer = new Developer();
        Set<Developer> developerSet = new HashSet<>();
        developerSet.add(developer);

        when(developerRepository.findAll()).thenReturn(developerSet);
        Set<DeveloperDTO> developers = developerService.getDevelopers();

        assertEquals(1, developers.size());
        verify(developerRepository, times(1)).findAll();
    }

    @Test
    public void findById() throws Exception {
        Developer developer = new Developer();
        developer.setId(1L);
        developer.setDeveloperName("Test");
        Optional<Developer> developerOptional = Optional.of(developer);

        when(developerRepository.findById(anyLong())).thenReturn(developerOptional);
        DeveloperDTO developerDTO = developerService.getById(1L);
        assertNotNull("DeveloperDTO not Found", developerDTO);
        verify(developerRepository, times(1)).findById(anyLong());
        verify(developerRepository, never()).findAll();
    }


    @Test
    public void deleteById() throws Exception {
        Long idTodelete = Long.valueOf(2L);

        developerRepository.deleteById(idTodelete);

        verify(developerRepository, times(1)).deleteById(anyLong());
    }

    @Test(expected = NotFoundException.class)
    public void getDeveloperByIdTestNotFound() throws Exception {
        Optional<Developer> developerOptional = Optional.empty();

        when(developerRepository.findById(anyLong())).thenReturn(developerOptional);

        developerService.getById(1L);
    }

    @Test
    public void getSortedDevelopersTest() {
        Developer developer = new Developer();
        developer.setId(1L);
        Set<Developer> developerSet = new HashSet<>();
        developerSet.add(developer);

        when(developerRepository.findAll()).thenReturn(developerSet);
        Set<DeveloperDTO> developers = developerService.getSortedDevelopers();

        assertEquals(1, developers.size());
        verify(developerRepository, times(1)).findAll();
    }

}
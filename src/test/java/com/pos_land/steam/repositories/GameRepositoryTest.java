package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Game;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class GameRepositoryTest {

    private static final Long GAME_ID = 1L;
    private static final String GAME_NAME = "GameName";

    @Mock
    private GameRepository gameRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindById() throws Exception {
        Game game = new Game();
        game.setId(GAME_ID);
        game.setGameName(GAME_NAME);

        Optional<Game> gameOptional = Optional.of(game);

        when(gameRepository.findById(anyLong())).thenReturn(gameOptional);

        Optional<Game> returnedDev = gameRepository.findById(GAME_ID);

        assertEquals(gameOptional.get().getId(), returnedDev.get().getId());
        assertEquals(gameOptional.get().getGameName(), returnedDev.get().getGameName());
        verify(gameRepository, times(1)).findById(anyLong());
        verify(gameRepository, never()).findAll();
    }

    @Test
    public void testFindAll() throws Exception {
        Game game = new Game();
        game.setId(GAME_ID);
        game.setGameName(GAME_NAME);

        List<Game> gameList = new ArrayList<>();
        gameList.add(game);

        when(gameRepository.findAll()).thenReturn(gameList);

        List<Game> returnedDevList = (List<Game>) gameRepository.findAll();

        assertEquals(gameList.iterator().next().getId(), returnedDevList.iterator().next().getId());
        assertEquals(gameList.iterator().next().getGameName(), returnedDevList.iterator().next().getGameName());
        assertEquals(gameList.size(), returnedDevList.size());
        verify(gameRepository, times(1)).findAll();
        verify(gameRepository, never()).findById(anyLong());

    }


}
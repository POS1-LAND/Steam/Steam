package com.pos_land.steam.repositories;

import com.pos_land.steam.converters.DeveloperConverter;
import com.pos_land.steam.domain.Developer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/static/testing-sql/create-dev.sql")
public class DeveloperRepositoryIT {

    private DeveloperConverter developerConverter;

    @Autowired
    private DeveloperRepository developerRepository;

    @Before
    public void setUp() throws Exception {


    }

    @Test
    public void testDeleteById() throws Exception {

        int size = ((ArrayList<Developer>) developerRepository.findAll()).size();

        developerRepository.deleteById(1L);
        Optional<Developer> deletedDevOptional = developerRepository.findById(1L);

        assertEquals(size - 1, ((ArrayList<Developer>) developerRepository.findAll()).size());
        assertEquals(false, deletedDevOptional.isPresent());

    }

    @Test
    public void testDeleteAll() throws Exception {

        assertNotEquals(0, ((ArrayList<Developer>) developerRepository.findAll()).size());

        developerRepository.deleteAll();

        assertEquals(0, ((ArrayList<Developer>) developerRepository.findAll()).size());

    }
}

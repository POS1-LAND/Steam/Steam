package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Developer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class DeveloperRepositoryTest {

    private static final Long DEV_ID = 1L;
    private static final String DEV_NAME = "DeveloperName";

    @Mock
    private DeveloperRepository developerRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindById() throws Exception {
        Developer developer = new Developer();
        developer.setId(DEV_ID);
        developer.setDeveloperName(DEV_NAME);

        Optional<Developer> developerOptional = Optional.of(developer);

        when(developerRepository.findById(anyLong())).thenReturn(developerOptional);

        Optional<Developer> returnedDev = developerRepository.findById(DEV_ID);

        assertEquals(developerOptional.get().getId(), returnedDev.get().getId());
        assertEquals(developerOptional.get().getDeveloperName(), returnedDev.get().getDeveloperName());
        verify(developerRepository, times(1)).findById(anyLong());
        verify(developerRepository, never()).findAll();
    }

    @Test
    public void testFindAll() throws Exception {
        Developer developer = new Developer();
        developer.setId(DEV_ID);
        developer.setDeveloperName(DEV_NAME);

        List<Developer> developerList = new ArrayList<>();
        developerList.add(developer);

        when(developerRepository.findAll()).thenReturn(developerList);

        List<Developer> returnedDevList = (List<Developer>) developerRepository.findAll();

        assertEquals(developerList.iterator().next().getId(), returnedDevList.iterator().next().getId());
        assertEquals(developerList.iterator().next().getDeveloperName(), returnedDevList.iterator().next().getDeveloperName());
        assertEquals(developerList.size(), returnedDevList.size());
        verify(developerRepository, times(1)).findAll();
        verify(developerRepository, never()).findById(anyLong());

    }

}
package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class UserRepositoryTest {

    @Mock
    private UserRepository userRepository;

    private static final Long USER_ID = 1L;
    private static final String USER_NAME = "User Name";


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindById() {
        User user = new User();
        user.setId(USER_ID);
        user.setUserName(USER_NAME);

        Optional<User> userOptional = Optional.of(user);

        when(userRepository.findById(anyLong())).thenReturn(userOptional);

        Optional<User> returnedUser = userRepository.findById(USER_ID);

        assertEquals(userOptional.get().getId(), returnedUser.get().getId());
        assertEquals(userOptional.get().getUserName(), returnedUser.get().getUserName());
        verify(userRepository, times(1)).findById(anyLong());
        verify(userRepository, never()).findAll();
    }

    @Test
    public void testFindAll() {
        User user = new User();
        user.setId(USER_ID);
        user.setUserName(USER_NAME);

        List<User> userArrayList = new ArrayList<>();
        userArrayList.add(user);

        when(userRepository.findAll()).thenReturn(userArrayList);

        ArrayList<User> returnedList = (ArrayList<User>) userRepository.findAll();

        assertEquals(userArrayList.size(), returnedList.size());
        assertEquals(userArrayList.get(0).getId(), returnedList.get(0).getId());
        assertEquals(userArrayList.get(0).getUserName(), returnedList.get(0).getUserName());
        verify(userRepository, times(1)).findAll();
        verify(userRepository, never()).findById(anyLong());
    }
}
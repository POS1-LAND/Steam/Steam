package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Language;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class LanguageRepositoryTest {

    private static final Long LANG_ID = 1L;

    @Mock
    LanguageRepository languageRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByIdTest() {
        Language language = new Language();
        language.setId(LANG_ID);

        Optional<Language> languageOptional = Optional.of(language);

        when(languageRepository.findById(anyLong())).thenReturn(languageOptional);

        Optional<Language> returnedLang = languageRepository.findById(LANG_ID);

        assertEquals(languageOptional.get().getId(), returnedLang.get().getId());
        verify(languageRepository, times(1)).findById(anyLong());
        verify(languageRepository, never()).findAll();
    }
}
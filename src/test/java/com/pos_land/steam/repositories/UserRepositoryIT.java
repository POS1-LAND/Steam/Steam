package com.pos_land.steam.repositories;

import com.pos_land.steam.converters.UserConverter;
import com.pos_land.steam.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/static/testing-sql/create-user.sql")
public class UserRepositoryIT {

    @Autowired
    private UserRepository userRepository;

    private UserConverter userConverter;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testDeleteById() {
        int size = ((ArrayList<User>) userRepository.findAll()).size();

        userRepository.deleteById(1L);
        Optional<User> deletedUserOptional = userRepository.findById(1L);

        assertEquals(size - 1, ((ArrayList<User>) userRepository.findAll()).size());
        assertEquals(false, deletedUserOptional.isPresent());
    }

    @Test
    public void testDeleteAll() {
        assertNotEquals(0, ((ArrayList<User>) userRepository.findAll()).size());

        userRepository.deleteAll();

        assertEquals(0, ((ArrayList<User>) userRepository.findAll()).size());
    }
}
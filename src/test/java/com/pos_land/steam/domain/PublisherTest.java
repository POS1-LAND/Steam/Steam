package com.pos_land.steam.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class PublisherTest {

    private Publisher publisher;

    @Before
    public void setUp() throws Exception {
        publisher = new Publisher();
    }

    @Test
    public void testAllGettersAndSetters() throws Exception {

        assertNull(publisher.getId());
        assertNull(publisher.getPublisherName());
        assertNotNull(publisher.getGames());
        assertEquals(0, publisher.getGames().size());

        publisher.setId(1L);
        publisher.setPublisherName("PublisherName");

        Set<Game> gameSet = new HashSet<>();
        Game game = new Game();
        gameSet.add(game);

        publisher.setGames(gameSet);

        assertNotNull(publisher);
        assertEquals(1L, publisher.getId().longValue());
        assertEquals("PublisherName", publisher.getPublisherName());
        assertEquals(1, publisher.getGames().size());
    }

}
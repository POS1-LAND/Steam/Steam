package com.pos_land.steam.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class GameTest {

    private static final Long GAME_ID = 1L;
    private static final String GAME_NAME = "Gamename";
    private Game game;

    @Before
    public void setUp() throws Exception {
        game = new Game();
    }

    @Test
    public void testAllGettersAndSetters() throws Exception {

        assertNull(game.getId());
        assertNull(game.getGameName());
        assertEquals(0, game.getDevelopers().size());

        game.setId(GAME_ID);
        game.setGameName(GAME_NAME);

        Set<Developer> developers = new HashSet<>();
        Developer developer = new Developer();
        developers.add(developer);

        game.setDevelopers(developers);

        assertNotNull(game);
        assertEquals(GAME_ID, game.getId());
        assertEquals(GAME_NAME, game.getGameName());
        assertEquals(1, game.getDevelopers().size());
    }

}
package com.pos_land.steam.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class LanguageTest {

    private Language language;

    @Before
    public void setUp() throws Exception {
        language = new Language();
    }

    @Test
    public void testAllGettersAndSetters() {
        assertNull(language.getId());
        assertNull(language.getLanguage());
        assertNotNull(language.getGames());
        assertEquals(0, language.getGames().size());

        language.setId(1L);
        language.setLanguage(ELanguage.ENGLISH);

        Set<Game> gameSet = new HashSet<>();
        Game game = new Game();
        gameSet.add(game);

        language.setGames(gameSet);

        assertEquals(1L, language.getId().longValue());
        assertEquals(ELanguage.ENGLISH, language.getLanguage());
        assertNotNull(language.getGames());
        assertEquals(1, language.getGames().size());
    }
}
package com.pos_land.steam.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class DeveloperTest {

    private static final Long DEV_ID = 1L;
    private Developer developer;

    @Before
    public void setUp() throws Exception {
        developer = new Developer();
    }

    @Test
    public void testAllGettersAndSetters() throws Exception {

        assertNull(developer.getId());
        assertNull(developer.getDeveloperName());
        assertEquals(0, developer.getGames().size());

        developer.setId(DEV_ID);
        developer.setDeveloperName("DeveloperName");

        Set<Game> gameSet = new HashSet<>();
        Game game = new Game();
        gameSet.add(game);

        developer.setGames(gameSet);

        assertNotNull(developer);
        assertEquals(DEV_ID, developer.getId());
        assertEquals("DeveloperName", developer.getDeveloperName());
        assertEquals(1, developer.getGames().size());
    }

}
package com.pos_land.steam.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class UserTest {

    private User user;

    private final static Long USER_ID = 1L;
    private final static String USER_NAME = "Name";
    private final static String USER_EMAIL = "Mail";
    private final static String USER_PW = "pw";


    @Before
    public void setUp() throws Exception {
        user = new User();
    }

    //To make sure Lombok is working
    @Test
    public void testAllGettersAndSetters() {

        assertNull(user.getId());
        assertNull(user.getUserName());
        assertEquals(0, user.getGames().size());
        assertNull(user.getEmail());
        assertNull(user.getPassword());

        user.setId(USER_ID);
        user.setUserName(USER_NAME);
        user.setPassword(USER_PW);
        user.setEmail(USER_EMAIL);

        Game game = new Game();
        Set<Game> gameSet = new HashSet<>();
        gameSet.add(game);

        user.setGames(gameSet);

        assertEquals(USER_ID, user.getId());
        assertEquals(USER_EMAIL, user.getEmail());
        assertEquals(USER_NAME, user.getUserName());
        assertEquals(USER_PW, user.getPassword());
        assertEquals(1, user.getGames().size());
    }
}
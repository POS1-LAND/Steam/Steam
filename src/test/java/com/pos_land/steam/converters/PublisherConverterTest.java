package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Game;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.PublisherDTO;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;


public class PublisherConverterTest {

    private PublisherConverter publisherConverter;

    @Before
    public void setUp() throws Exception {
        publisherConverter = new PublisherConverter(new SimpleGameConverter());
    }

    @Test
    public void testNullObject() {
        PublisherDTO publisherDTO = null;
        Publisher publisher = null;
        assertNull(publisherConverter.convert(publisherDTO));
        assertNull(publisherConverter.convert(publisher));
    }

    @Test
    public void testEmptyObject() {
        Publisher publisher = new Publisher();
        PublisherDTO publisherDTO = new PublisherDTO();

        assertNotNull(publisherConverter.convert(publisher));
        assertNotNull(publisherConverter.convert(publisherDTO));
    }

    @Test
    public void testConvertPublisher() throws Exception {
        Publisher publisher = new Publisher();
        publisher.setId(1L);
        publisher.setPublisherName("Test");
        Set<Game> gameSet = new HashSet<>();
        gameSet.add(new Game());
        gameSet.add(new Game());

        publisher.setGames(gameSet);

        PublisherDTO publisherDTO = publisherConverter.convert(publisher);

        assertNotNull(publisherDTO);
        assertEquals(publisher.getId(), publisherDTO.getId());
        assertEquals(publisher.getPublisherName(), publisherDTO.getPublisherName());
        assertNotEquals(0, publisherDTO.getGames().size());
    }

    @Test
    public void testConvertPublisherDTO() throws Exception {
        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(1L);
        publisherDTO.setPublisherName("Test");

        Set<GameDTO> games = new HashSet<>();
        games.add(new GameDTO());
        games.add(new GameDTO());

        publisherDTO.setGames(games);

        Publisher publisher = publisherConverter.convert(publisherDTO);

        assertNotNull(publisherDTO);
        assertEquals(publisherDTO.getId(), publisher.getId());
        assertEquals(publisherDTO.getPublisherName(), publisher.getPublisherName());
        assertNotEquals(0, publisher.getGames().size());
    }

}
package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class DeveloperConverterTest {

    private DeveloperConverter developerConverter;

    @Mock
    private SimpleGameConverter simpleGameConverter;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        developerConverter = new DeveloperConverter(simpleGameConverter);
    }

    @Test
    public void testNullObject() throws Exception {
        Developer developer = null;
        DeveloperDTO developerDTO = null;
        assertNull(developerConverter.convert(developer));
        assertNull(developerConverter.convert(developerDTO));
    }

    @Test
    public void testEmptyObject() throws Exception {
        assertNotNull(developerConverter.convert(new DeveloperDTO()));
        assertNotNull(developerConverter.convert(new Developer()));
    }

    @Test
    public void testConvertDeveloper() throws Exception {
        Developer developer = new Developer();
        developer.setId(1L);
        developer.setDeveloperName("DeveloperName");

        Set<Game> games = new HashSet<>();
        games.add(new Game());
        games.add(new Game());
        developer.setGames(games);

        DeveloperDTO convertedDev = developerConverter.convert(developer);

        assertEquals(developer.getId(), convertedDev.getId());
        assertEquals(developer.getDeveloperName(), convertedDev.getDeveloperName());
        assertNotEquals(0, convertedDev.getGames().size());
    }

    @Test
    public void testConvertDeveloperDTO() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);
        developerDTO.setDeveloperName("DeveloperName");

        Set<GameDTO> games = new HashSet<>();
        games.add(new GameDTO());
        games.add(new GameDTO());
        developerDTO.setGames(games);

        Developer convertedDev = developerConverter.convert(developerDTO);

        assertEquals(developerDTO.getId(), convertedDev.getId());
        assertEquals(developerDTO.getDeveloperName(), convertedDev.getDeveloperName());
        assertNotEquals(0, convertedDev.getGames().size());
    }

}
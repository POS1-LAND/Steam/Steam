package com.pos_land.steam.converters;

import com.pos_land.steam.domain.*;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.dto.PublisherDTO;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class GameConverterTest {

    private GameConverter gameConverter;
    private SimpleDeveloperConverter developerConverter;
    private LanguageConverter languageConverter;
    private SimplePublisherConverter publisherConverter;


    static final Long GAME_ID = 1L;
    static final String GAME_NAME = "GameName";
    static final String GAME_DESC = "Game Description";
    static final EGenre GAME_GENRE = EGenre.ADVENTURE;
    static final Double GAME_PRICE = 59.99;
    static final Date GAME_RELEASEDATE = new Date();


    static final Long DEVELOPER_ID = 3L;
    static final String DEVELOPER_NAME = "Developer Guy";

    static final Long LANG_ID = 1L;
    static final ELanguage LANG_ELANG = ELanguage.ENGLISH;

    static final Long PUB_ID = 2L;
    static final String PUB_NAME = "Publisher Guy";


    @Before
    public void setUp() throws Exception {
        developerConverter = new SimpleDeveloperConverter();
        languageConverter = new LanguageConverter();
        publisherConverter = new SimplePublisherConverter();
        gameConverter = new GameConverter(developerConverter, publisherConverter, languageConverter);
    }

    @Test
    public void testNullObject() {
        GameDTO gameDTO = null;
        Game game = null;
        assertNull(gameConverter.convert(gameDTO));
        assertNull(gameConverter.convert(game));
    }

    @Test
    public void testEmptyObject() {
        Game game = new Game();
        game.setPublisher(new Publisher());
        GameDTO gameDTO = new GameDTO();
        gameDTO.setPublisher(new PublisherDTO());

        assertNotNull(gameConverter.convert(game));
        assertNotNull(gameConverter.convert(gameDTO));
    }

    @Test
    public void testConvertGame() {
        Game game = new Game();
        game.setId(GAME_ID);
        game.setGameName(GAME_NAME);
        game.setDescription(GAME_DESC);
        game.setGenre(GAME_GENRE);
        game.setPrice(GAME_PRICE);
        game.setReleaseDate(GAME_RELEASEDATE);

        Set<Developer> developers = new HashSet<>();
        Developer developer = new Developer();
        developer.setId(DEVELOPER_ID);
        developer.setDeveloperName(DEVELOPER_NAME);
        developer.getGames().add(game);
        developers.add(developer);

        Set<Language> languages = new HashSet<>();
        Language englishLang = new Language();
        englishLang.setId(LANG_ID);
        englishLang.setLanguage(LANG_ELANG);
        englishLang.getGames().add(game);
        languages.add(englishLang);

        Publisher publisher = new Publisher();
        publisher.setId(PUB_ID);
        publisher.setPublisherName(PUB_NAME);
        publisher.getGames().add(game);

        game.setDevelopers(developers);
        game.setLanguages(languages);
        game.setPublisher(publisher);

        //when
        GameDTO gameDTO = gameConverter.convert(game);

        assertNotNull(gameDTO);
        assertEquals(GAME_ID, gameDTO.getId());
        assertEquals(GAME_DESC, gameDTO.getDescription());
        assertEquals(GAME_GENRE, gameDTO.getGenre());
        assertEquals(GAME_NAME, gameDTO.getGameName());
        assertEquals(GAME_PRICE, gameDTO.getPrice());
        assertEquals(GAME_RELEASEDATE, gameDTO.getReleaseDate());
        assertEquals(PUB_ID, gameDTO.getPublisher().getId());
        assertEquals(PUB_NAME, gameDTO.getPublisher().getPublisherName());
        assertEquals(1,gameDTO.getDevelopers().size());
        assertEquals(1,gameDTO.getLanguages().size());
    }

    @Test
    public void testConvertGameDTO() {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setGameName(GAME_NAME);
        gameDTO.setDescription(GAME_DESC);
        gameDTO.setGenre(GAME_GENRE);
        gameDTO.setPrice(GAME_PRICE);
        gameDTO.setReleaseDate(GAME_RELEASEDATE);

        Set<DeveloperDTO> developers = new HashSet<>();
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(DEVELOPER_ID);
        developerDTO.setDeveloperName(DEVELOPER_NAME);
        developers.add(developerDTO);

        Set<LanguageDTO> languages = new HashSet<>();
        LanguageDTO englishLang = new LanguageDTO();
        englishLang.setId(LANG_ID);
        englishLang.setLanguage(LANG_ELANG);
        languages.add(englishLang);

        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(PUB_ID);
        publisherDTO.setPublisherName(PUB_NAME);

        gameDTO.setDevelopers(developers);
        gameDTO.setLanguages(languages);
        gameDTO.setPublisher(publisherDTO);

        //when
        Game game = gameConverter.convert(gameDTO);

        assertNotNull(game);
        assertEquals(GAME_ID, game.getId());
        assertEquals(GAME_DESC, game.getDescription());
        assertEquals(GAME_GENRE, game.getGenre());
        assertEquals(GAME_NAME, game.getGameName());
        assertEquals(GAME_PRICE, game.getPrice());
        assertEquals(GAME_RELEASEDATE, game.getReleaseDate());
        assertEquals(PUB_ID, game.getPublisher().getId());
        assertEquals(PUB_NAME, game.getPublisher().getPublisherName());
        assertEquals(1,game.getDevelopers().size());
        assertEquals(1,game.getLanguages().size());
    }
}
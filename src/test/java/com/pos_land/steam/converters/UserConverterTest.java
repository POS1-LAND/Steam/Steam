package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Game;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.domain.User;
import com.pos_land.steam.dto.UserDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class UserConverterTest {

    @Mock
    private GameConverter gameConverter;

    private UserConverter userConverter;

    private final static Long USER_ID = 2L;
    private final static String USER_NAME = "User";
    private final static String USER_EMAIL = "whatever@aa.at";
    private final static String USER_PW = "dfklsjfds";


    private final static Long GAME_ID = 1L;
    private final static String GAME_NAME = "Game";


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userConverter = new UserConverter(gameConverter);
    }

    @Test
    public void testNullObject() {
        UserDTO userDTO = null;
        User user = null;
        assertNull(userConverter.convert(user));
        assertNull(userConverter.convert(userDTO));
    }

    @Test
    public void testEmptyObject() {
        assertNotNull(userConverter.convert(new User()));
        assertNotNull(userConverter.convert(new UserDTO()));
    }

    @Test
    public void testConvertUser() {
        User user = new User();
        user.setId(USER_ID);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PW);
        user.setUserName(USER_NAME);

        Game game = new Game();
        game.setGameName(GAME_NAME);
        game.setId(GAME_ID);
        game.setPublisher(new Publisher());

        user.getGames().add(game);

        UserDTO userDTO = userConverter.convert(user);

        assertNotNull(userDTO);
        assertEquals(USER_ID, userDTO.getId());
        assertEquals(USER_EMAIL, userDTO.getEmail());
        assertEquals(USER_NAME, userDTO.getUserName());
        assertEquals(USER_PW, userDTO.getPassword());
        assertEquals(1, userDTO.getGames().size());
    }

    @Test
    public void testConvertUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(USER_ID);
        userDTO.setEmail(USER_EMAIL);
        userDTO.setPassword(USER_PW);
        userDTO.setUserName(USER_NAME);

        Game game = new Game();
        game.setGameName(GAME_NAME);
        game.setId(GAME_ID);
        game.setPublisher(new Publisher());

        userDTO.getGames().add(gameConverter.convert(game));

        User user = userConverter.convert(userDTO);

        assertNotNull(user);
        assertEquals(USER_ID, user.getId());
        assertEquals(USER_EMAIL, user.getEmail());
        assertEquals(USER_NAME, user.getUserName());
        assertEquals(USER_PW, user.getPassword());
        assertEquals(1, user.getGames().size());
    }
}
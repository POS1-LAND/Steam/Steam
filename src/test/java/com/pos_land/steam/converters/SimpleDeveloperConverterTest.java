package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class SimpleDeveloperConverterTest {

    private SimpleDeveloperConverter simpleDeveloperConverter;

    @Before
    public void setUp() throws Exception {
        simpleDeveloperConverter = new SimpleDeveloperConverter();
    }

    @Test
    public void testNullObject() {
        Developer developer = null;
        DeveloperDTO developerDTO = null;

        assertNull(simpleDeveloperConverter.convert(developer));
        assertNull(simpleDeveloperConverter.convert(developerDTO));
    }

    @Test
    public void testEmptyObject() throws Exception {
        assertNotNull(simpleDeveloperConverter.convert(new DeveloperDTO()));
        assertNotNull(simpleDeveloperConverter.convert(new Developer()));
    }

    @Test
    public void testConvertDeveloper() throws Exception {
        Developer developer = new Developer();
        developer.setId(1L);
        developer.setDeveloperName("DeveloperName");

        Set<Game> games = new HashSet<>();
        games.add(new Game());
        games.add(new Game());
        developer.setGames(games);

        DeveloperDTO convertedDev = simpleDeveloperConverter.convert(developer);

        assertEquals(developer.getId(), convertedDev.getId());
        assertEquals(developer.getDeveloperName(), convertedDev.getDeveloperName());
        assertEquals(0, convertedDev.getGames().size());
    }

    @Test
    public void testConvertDeveloperDTO() throws Exception {
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(1L);
        developerDTO.setDeveloperName("DeveloperName");

        Set<GameDTO> games = new HashSet<>();
        games.add(new GameDTO());
        games.add(new GameDTO());
        developerDTO.setGames(games);

        Developer convertedDev = simpleDeveloperConverter.convert(developerDTO);

        assertEquals(developerDTO.getId(), convertedDev.getId());
        assertEquals(developerDTO.getDeveloperName(), convertedDev.getDeveloperName());
        assertEquals(0, convertedDev.getGames().size());
    }
}
package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.domain.Language;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.dto.PublisherDTO;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.pos_land.steam.converters.GameConverterTest.*;
import static org.junit.Assert.*;

public class SimpleGameConverterTest {

    private SimpleGameConverter simpleGameConverter;

    @Before
    public void setUp() throws Exception {
        simpleGameConverter = new SimpleGameConverter();
    }

    @Test
    public void testNullObject() {
        GameDTO gameDTO = null;
        Game game = null;
        assertNull(simpleGameConverter.convert(gameDTO));
        assertNull(simpleGameConverter.convert(game));
    }

    @Test
    public void testEmptyObject() {
        assertNotNull(simpleGameConverter.convert(new Game()));
        assertNotNull(simpleGameConverter.convert(new GameDTO()));
    }

    @Test
    public void testConvertGame() {
        Game game = new Game();
        game.setId(GAME_ID);
        game.setGameName(GAME_NAME);
        game.setDescription(GAME_DESC);
        game.setGenre(GAME_GENRE);
        game.setPrice(GAME_PRICE);
        game.setReleaseDate(GAME_RELEASEDATE);

        Set<Developer> developers = new HashSet<>();
        Developer developer = new Developer();
        developer.setId(DEVELOPER_ID);
        developer.setDeveloperName(DEVELOPER_NAME);
        developer.getGames().add(game);
        developers.add(developer);

        Set<Language> languages = new HashSet<>();
        Language englishLang = new Language();
        englishLang.setId(LANG_ID);
        englishLang.setLanguage(LANG_ELANG);
        englishLang.getGames().add(game);
        languages.add(englishLang);

        Publisher publisher = new Publisher();
        publisher.setId(PUB_ID);
        publisher.setPublisherName(PUB_NAME);
        publisher.getGames().add(game);

        game.setDevelopers(developers);
        game.setLanguages(languages);
        game.setPublisher(publisher);

        //when
        GameDTO gameDTO = simpleGameConverter.convert(game);

        assertNotNull(gameDTO);
        assertEquals(GAME_ID, gameDTO.getId());
        assertEquals(GAME_DESC, gameDTO.getDescription());
        assertEquals(GAME_GENRE, gameDTO.getGenre());
        assertEquals(GAME_NAME, gameDTO.getGameName());
        assertEquals(GAME_PRICE, gameDTO.getPrice());
        assertEquals(GAME_RELEASEDATE, gameDTO.getReleaseDate());
        assertNull(gameDTO.getPublisher());
        assertNotEquals(1, gameDTO.getDevelopers().size());
        assertNotEquals(1, gameDTO.getLanguages().size());
    }

    @Test
    public void testConvertGameDTO() {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(GAME_ID);
        gameDTO.setGameName(GAME_NAME);
        gameDTO.setDescription(GAME_DESC);
        gameDTO.setGenre(GAME_GENRE);
        gameDTO.setPrice(GAME_PRICE);
        gameDTO.setReleaseDate(GAME_RELEASEDATE);

        Set<DeveloperDTO> developers = new HashSet<>();
        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(DEVELOPER_ID);
        developerDTO.setDeveloperName(DEVELOPER_NAME);
        developers.add(developerDTO);

        Set<LanguageDTO> languages = new HashSet<>();
        LanguageDTO englishLang = new LanguageDTO();
        englishLang.setId(LANG_ID);
        englishLang.setLanguage(LANG_ELANG);
        languages.add(englishLang);

        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(PUB_ID);
        publisherDTO.setPublisherName(PUB_NAME);

        gameDTO.setDevelopers(developers);
        gameDTO.setLanguages(languages);
        gameDTO.setPublisher(publisherDTO);

        //when
        Game game = simpleGameConverter.convert(gameDTO);

        assertNotNull(game);
        assertEquals(GAME_ID, game.getId());
        assertEquals(GAME_DESC, game.getDescription());
        assertEquals(GAME_GENRE, game.getGenre());
        assertEquals(GAME_NAME, game.getGameName());
        assertEquals(GAME_PRICE, game.getPrice());
        assertEquals(GAME_RELEASEDATE, game.getReleaseDate());
        assertNull(game.getPublisher());
        assertNotEquals(1, game.getDevelopers().size());
        assertNotEquals(1, game.getLanguages().size());
    }
}
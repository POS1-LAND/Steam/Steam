package com.pos_land.steam.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Language {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private ELanguage language;

    @ManyToMany(mappedBy = "languages")
    private Set<Game> games = new HashSet<>();

    public Language(ELanguage language) {
        this.language = language;
    }


}

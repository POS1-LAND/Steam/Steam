package com.pos_land.steam.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Lob
    private String description;
    private String gameName;
    private Double price;

    /* handles data-binding (parsing) and display if spring form tld or spring:eval */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date releaseDate;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "game_developer",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "developer_id"))
    private Set<Developer> developers = new HashSet<>();

    @ManyToOne
    private Publisher publisher;

    @ManyToMany
    @JoinTable(name = "game_language",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "language_id"))
    private Set<Language> languages = new HashSet<>();

    @Enumerated(value = EnumType.STRING)
    private EGenre genre;

    public Game(String description, String gameName, Double price, Date releaseDate) {

        this.description = description;
        this.gameName = gameName;
        this.price = price;
        this.releaseDate = releaseDate;
    }

    public Game() {
    }

    public void addDeveloper(Developer developer) {
        this.getDevelopers().add(developer);
    }

}

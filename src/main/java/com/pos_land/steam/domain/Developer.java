package com.pos_land.steam.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
public class Developer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String developerName;

    @ManyToMany(mappedBy = "developers")
    Set<Game> games = new HashSet<>();

    public Developer(String developerName) {
        this.developerName = developerName;
    }

    public Developer() {
    }
}

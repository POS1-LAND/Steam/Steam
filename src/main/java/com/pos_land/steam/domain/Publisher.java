package com.pos_land.steam.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String publisherName;

    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "publisher")
    Set<Game> games = new HashSet<>();

    public Publisher(String publisherName) {
        this.publisherName = publisherName;
    }

    public Publisher() {
    }

}

package com.pos_land.steam.repositories;

import com.pos_land.steam.domain.Game;
import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Long> {
}

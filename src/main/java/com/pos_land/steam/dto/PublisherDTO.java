package com.pos_land.steam.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class PublisherDTO implements Comparable<PublisherDTO> {

    private Long id;
    private String publisherName;
    private Set<GameDTO> games = new HashSet<>();

    @Override
    public int compareTo(PublisherDTO publisher) {
        return (this.getId() < publisher.getId() ? -1 :
                (this.getId() == publisher.getId()) ? 0 : 1);
    }

}

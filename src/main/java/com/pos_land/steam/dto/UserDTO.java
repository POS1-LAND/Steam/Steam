package com.pos_land.steam.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Comparable<UserDTO> {

    private Long id;
    private String userName;
    private String email;
    private String password;
    private Set<GameDTO> games = new HashSet<>();

    @Override
    public int compareTo(UserDTO userDTO) {
        return (this.getId() < userDTO.getId() ? -1 :
                (this.getId() == userDTO.getId()) ? 0 : 1);
    }
}

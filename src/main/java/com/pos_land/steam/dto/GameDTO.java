package com.pos_land.steam.dto;

import com.pos_land.steam.domain.EGenre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class GameDTO implements Comparable<GameDTO> {

    private Long id;
    private String description;
    private String gameName;
    private Double price;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date releaseDate;

    private Set<DeveloperDTO> developers = new HashSet<>();
    private PublisherDTO publisher;
    private Set<LanguageDTO> languages = new HashSet<>();
    private EGenre genre;

    @Override
    public int compareTo(GameDTO gameDTO) {
        return (this.getId() < gameDTO.getId() ? -1 :
                (this.getId() == gameDTO.getId()) ? 0 : 1);
    }
}

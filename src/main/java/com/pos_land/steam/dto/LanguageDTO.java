package com.pos_land.steam.dto;

import com.pos_land.steam.domain.ELanguage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class LanguageDTO {

    private Long id;
    private ELanguage language;
//    private Set<GameDTO> games = new HashSet<>();

}

package com.pos_land.steam.services;

import com.pos_land.steam.dto.DeveloperDTO;

import java.util.Set;

public interface DeveloperService {

    Set<DeveloperDTO> getDevelopers();

    Set<DeveloperDTO> getSortedDevelopers();

    DeveloperDTO getById(Long id);

    DeveloperDTO saveDeveloper(DeveloperDTO developer);

    void deleteById(Long id);

    Set<DeveloperDTO> getSelectedDevelopers(Set<DeveloperDTO> domainDevelopers);
}

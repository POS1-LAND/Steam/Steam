package com.pos_land.steam.services;

import com.pos_land.steam.dto.LanguageDTO;

import java.util.Set;

public interface LanguageService {

    LanguageDTO getById(Long id);

    Set<LanguageDTO> getLanguages();

    Set<LanguageDTO> getSortedLanguages();

    Set<LanguageDTO> getDisplayedLanguages(Set<LanguageDTO> domainGames);
}

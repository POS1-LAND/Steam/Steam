package com.pos_land.steam.services;

import com.pos_land.steam.dto.GameDTO;

import java.util.Set;

public interface GameService {

    Set<GameDTO> getGames();

    Set<GameDTO> getSortedGames();

    GameDTO getById(Long id);

    GameDTO saveGame(GameDTO game);

    void deleteById(Long id);

    Set<GameDTO> getDisplayedGames(Set<GameDTO> domainGames);
}

package com.pos_land.steam.services;

import com.pos_land.steam.dto.UserDTO;

import java.util.Set;

public interface UserService {

    Set<UserDTO> getUsers();

    Set<UserDTO> getSortedUsers();

    UserDTO getById(Long id);

    UserDTO saveUser(UserDTO user);

    void deleteById(Long id);

}

package com.pos_land.steam.services;

import com.pos_land.steam.converters.PublisherConverter;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.PublisherDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.PublisherRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PublisherServiceImpl implements PublisherService {

    private final PublisherRepository publisherRepository;
    private final PublisherConverter publisherConverter;

    public PublisherServiceImpl(PublisherRepository publisherRepository, PublisherConverter publisherConverter) {
        this.publisherRepository = publisherRepository;
        this.publisherConverter = publisherConverter;
    }

    @Override
    public Set<PublisherDTO> getPublishers() {

        Set<PublisherDTO> publisherDTOSet = new HashSet<>();
        publisherRepository.findAll().iterator().forEachRemaining(publisher -> publisherDTOSet.add(publisherConverter.convert(publisher)));


        return publisherDTOSet;
    }

    @Override
    public Set<PublisherDTO> getSortedPublishers() {

        SortedSet<PublisherDTO> publisherDTOSet = new TreeSet<>(Comparator.comparing(PublisherDTO::getId));
        publisherRepository.findAll().iterator().forEachRemaining(publisher -> publisherDTOSet.add(publisherConverter.convert(publisher)));


        return publisherDTOSet;
    }

    @Override
    public PublisherDTO getById(Long id) {

        Optional<Publisher> publisherOptional = publisherRepository.findById(id);

        if(!publisherOptional.isPresent()){
            throw new NotFoundException("Publisher with ID " + id + " not found!");
        }

        return publisherConverter.convert(publisherOptional.get());
    }

    @Override
    public PublisherDTO savePublisher(PublisherDTO publisherDTO) {
        Publisher detachedPublisher = publisherConverter.convert(publisherDTO);
        Publisher savedPublisher = publisherRepository.save(detachedPublisher);
        return publisherConverter.convert(savedPublisher);
    }

    @Override
    public void deleteById(Long id) {
        publisherRepository.deleteById(id);
    }
}

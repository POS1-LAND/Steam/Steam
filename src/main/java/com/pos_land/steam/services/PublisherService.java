package com.pos_land.steam.services;

import com.pos_land.steam.dto.PublisherDTO;

import java.util.Set;

public interface PublisherService {

    Set<PublisherDTO> getPublishers();

    Set<PublisherDTO> getSortedPublishers();

    PublisherDTO getById(Long id);

    PublisherDTO savePublisher(PublisherDTO publisherDTO);

    void deleteById(Long id);
}

package com.pos_land.steam.services;

import com.pos_land.steam.converters.DeveloperConverter;
import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.DeveloperRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    private final DeveloperRepository developerRepository;
    private final DeveloperConverter developerConverter;

    public DeveloperServiceImpl(DeveloperRepository developerRepository, DeveloperConverter developerConverter) {
        this.developerRepository = developerRepository;
        this.developerConverter = developerConverter;
    }

    @Override
    public Set<DeveloperDTO> getDevelopers() {
        Set<DeveloperDTO> developers = new HashSet<>();

        developerRepository.findAll().iterator().forEachRemaining(developer -> developers.add(developerConverter.convert(developer)));

        return developers;
    }

    @Override
    public Set<DeveloperDTO> getSortedDevelopers() {
        SortedSet<DeveloperDTO> developers = new TreeSet<>(Comparator.comparing(DeveloperDTO::getId));
        developerRepository.findAll().iterator().forEachRemaining(developer -> developers.add(developerConverter.convert(developer)));

        return developers;

    }

    @Override
    public DeveloperDTO getById(Long id) {

        Optional<Developer> developerOptional = developerRepository.findById(id);

        if (!developerOptional.isPresent()) {
            throw new NotFoundException("Developer with ID " + id + " not found!");
        }

        return developerConverter.convert(developerOptional.get());
    }

    @Override
    public DeveloperDTO saveDeveloper(DeveloperDTO developer) {
        Developer detachedDeveloper = developerConverter.convert(developer);

        Developer savedDeveloper = developerRepository.save(detachedDeveloper);

        return developerConverter.convert(savedDeveloper);
    }

    @Override
    public void deleteById(Long id) {

        developerRepository.deleteById(id);

    }

    @Override
    public Set<DeveloperDTO> getSelectedDevelopers(Set<DeveloperDTO> domainDevelopers) {
        Set<DeveloperDTO> allDevelopers = getDevelopers();
        Set<DeveloperDTO> selectedDevelopers = new HashSet<>();

        allDevelopers.forEach(developerDTO -> {
            selectedDevelopers.add(developerDTO);

            domainDevelopers.forEach(dev -> {
                if (dev.getId().longValue() == developerDTO.getId()) {
                    selectedDevelopers.remove(developerDTO);
                }
            });
        });

        return selectedDevelopers;
    }
}

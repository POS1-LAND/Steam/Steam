package com.pos_land.steam.services;

import com.pos_land.steam.converters.LanguageConverter;
import com.pos_land.steam.domain.Language;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.LanguageRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final LanguageRepository languageRepository;
    private final LanguageConverter languageConverter;

    public LanguageServiceImpl(LanguageRepository languageRepository, LanguageConverter languageConverter) {
        this.languageRepository = languageRepository;
        this.languageConverter = languageConverter;
    }

    @Override

    public LanguageDTO getById(Long id) {
        Optional<Language> languageOptional = languageRepository.findById(id);

        if (!languageOptional.isPresent()) {
            throw new NotFoundException("Language with ID " + id + " not found!");
        }

        return languageConverter.convert(languageOptional.get());
    }

    @Override
    public Set<LanguageDTO> getLanguages() {
        Set<LanguageDTO> languageDTOS = new HashSet<>();

        languageRepository.findAll().forEach(language -> languageDTOS.add(languageConverter.convert(language)));

        return languageDTOS;
    }

    @Override
    public Set<LanguageDTO> getSortedLanguages() {
        SortedSet<LanguageDTO> languageDTOS = new TreeSet<>(Comparator.comparing(LanguageDTO::getId));

        languageRepository.findAll().forEach(language -> languageDTOS.add(languageConverter.convert(language)));

        return languageDTOS;
    }

    @Override
    public Set<LanguageDTO> getDisplayedLanguages(Set<LanguageDTO> domainLanguages) {

        Set<LanguageDTO> allLanguages = getLanguages();
        Set<LanguageDTO> displayedLanguages = new HashSet<>();
        allLanguages.iterator().forEachRemaining(languageDTO -> {
            displayedLanguages.add(languageDTO);

            domainLanguages.forEach(language -> {
                if (language.getId().longValue() == languageDTO.getId()) {
                    displayedLanguages.remove(languageDTO);
                }
            });
        });

        return displayedLanguages;
    }
}

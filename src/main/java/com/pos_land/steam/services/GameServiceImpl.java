package com.pos_land.steam.services;

import com.pos_land.steam.converters.GameConverter;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.GameRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;
    private final GameConverter gameConverter;

    public GameServiceImpl(GameRepository gameRepository, GameConverter gameConverter) {
        this.gameRepository = gameRepository;
        this.gameConverter = gameConverter;
    }

    @Override
    public Set<GameDTO> getGames() {
        Set<GameDTO> gameSet = new HashSet<>();

        gameRepository.findAll().iterator().forEachRemaining(game -> gameSet.add(gameConverter.convert(game)));

        return gameSet;
    }

    @Override
    public Set<GameDTO> getSortedGames() {
        SortedSet<GameDTO> gameSet = new TreeSet<>(Comparator.comparing(GameDTO::getId));

        gameRepository.findAll().iterator().forEachRemaining(game -> gameSet.add(gameConverter.convert(game)));

        return gameSet;
    }


    @Override
    public GameDTO getById(Long id) {

        Optional<Game> game = gameRepository.findById(id);

        if (!game.isPresent()) {
            throw new NotFoundException("Game with ID " + id + " not found!");
        }

        return gameConverter.convert(game.get());
    }

    @Override
    public GameDTO saveGame(GameDTO game) {

        Game detachedGame = gameConverter.convert(game);

        Game savedGame = gameRepository.save(detachedGame);

        return gameConverter.convert(savedGame);
    }

    @Override
    public void deleteById(Long id) {

        gameRepository.deleteById(id);
    }

    @Override
    public Set<GameDTO> getDisplayedGames(Set<GameDTO> domainGames) {

        Set<GameDTO> allGames = getGames();
        Set<GameDTO> displayedGames = new HashSet<>();
        allGames.iterator().forEachRemaining(gameDTO -> {
            displayedGames.add(gameDTO);

            domainGames.forEach(game -> {
                if (game.getId() == gameDTO.getId()) {
                    displayedGames.remove(gameDTO);
                }
            });
        });

        return displayedGames;
    }
}

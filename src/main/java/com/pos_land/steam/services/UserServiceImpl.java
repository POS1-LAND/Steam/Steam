package com.pos_land.steam.services;

import com.pos_land.steam.converters.UserConverter;
import com.pos_land.steam.domain.User;
import com.pos_land.steam.dto.UserDTO;
import com.pos_land.steam.exceptions.NotFoundException;
import com.pos_land.steam.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserConverter userConverter;

    public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Override
    public Set<UserDTO> getUsers() {
        Set<UserDTO> userDTOSet = new HashSet<>();

        userRepository.findAll().iterator().forEachRemaining(userDomain -> userDTOSet.add(userConverter.convert(userDomain)));

        return userDTOSet;
    }

    @Override
    public Set<UserDTO> getSortedUsers() {
        SortedSet<UserDTO> userDTOSet = new TreeSet<>(Comparator.comparing(UserDTO::getId));

        userRepository.findAll().iterator().forEachRemaining(userDomain -> userDTOSet.add(userConverter.convert(userDomain)));

        return userDTOSet;
    }

    @Override
    public UserDTO getById(Long id) {
        //get user domain and return converted dto
        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new NotFoundException("User with ID " + id + " not found!");
        }

        return userConverter.convert(userOptional.get());
    }

    @Override
    public UserDTO saveUser(UserDTO user) {
        User detachedUser = userConverter.convert(user);

        User savedUser = userRepository.save(detachedUser);

        return userConverter.convert(savedUser);
    }

    @Override
    public void deleteById(Long id) {

        userRepository.deleteById(id);

    }
}

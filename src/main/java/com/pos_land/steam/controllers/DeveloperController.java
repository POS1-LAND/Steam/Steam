package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.services.DeveloperService;
import com.pos_land.steam.services.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Slf4j
@Controller
public class DeveloperController {

    private final static String DEV_UPDATEFORM = "developer/update";
    private final static String DEV_CREATEFORM = "developer/create";
    private final DeveloperService developerService;
    private final GameService gameService;

    public DeveloperController(DeveloperService developerService, GameService gameService) {
        this.developerService = developerService;
        this.gameService = gameService;
    }

    @RequestMapping("/developers")
    public String getDevelopers(Model model) {

        model.addAttribute("developers", developerService.getSortedDevelopers());
        return "developer";
    }

    @RequestMapping("developer/{id}/view")
    public String getDeveloperById(@PathVariable String id, Model model) {

        model.addAttribute("developer", developerService.getById(Long.valueOf(id)));

        return "developer/view";
    }

    @RequestMapping("developer/create")
    public String createDeveloper(Model model) {

        model.addAttribute("developer", new DeveloperDTO());
        return "developer/create";

    }

    @PostMapping("developer/create/submit")
    public String create(@Valid @ModelAttribute DeveloperDTO developerDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> log.debug(objectError.toString()));
            return DEV_CREATEFORM;
        }

        DeveloperDTO savedDev = developerService.saveDeveloper(developerDTO);

        return "redirect:/developer/" + savedDev.getId() + "/view";
    }

    @RequestMapping("developer/{id}/update")
    public String updateDeveloper(@PathVariable String id, Model model) {

        DeveloperDTO developerDTO = developerService.getById(Long.valueOf(id));

        model.addAttribute("developer", developerDTO);

        // allGames attribute for a drop-down list of all existing games to easily add games to publishers
        Set<GameDTO> displayedGames = gameService.getDisplayedGames(developerDTO.getGames());
        SortedSet<GameDTO> sortedGameSet = new TreeSet<>(Comparator.comparing(GameDTO::getId));
        sortedGameSet.addAll(displayedGames);
        model.addAttribute("allGames", sortedGameSet);

        return DEV_UPDATEFORM;
    }

    @PostMapping("developer/update/submit")
    public String update(@Valid @ModelAttribute DeveloperDTO developerDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> log.debug(objectError.toString()));

            return DEV_UPDATEFORM;
        }

        DeveloperDTO savedDeveloper = developerService.saveDeveloper(developerDTO);

        return "redirect:/developer/" + savedDeveloper.getId() + "/view";
    }

    @GetMapping("developer/{id}/delete")
    public String deleteDeveloper(@PathVariable String id) {

        developerService.deleteById(Long.valueOf(id));

        return "redirect:/developers";
    }

    @GetMapping("/developer/{devId}/game/{gameId}/delete")
    public String deleteGameFromDeveloper(@PathVariable Long devId, @PathVariable Long gameId) {
        GameDTO gameToDelete = gameService.getById(gameId);
        Set<DeveloperDTO> developerDTOSet = gameToDelete.getDevelopers();

        for (DeveloperDTO developer : developerDTOSet) {
            if(developer.getId() == devId){
                gameToDelete.getDevelopers().remove(developer);
                break;
            }
        }
        gameToDelete.getDevelopers().remove(developerService.getById(devId));
        gameService.saveGame(gameToDelete);

        return "redirect:/developer/" + devId + "/update";
    }

    @GetMapping("/developer/{devId}/game/{gameId}/add")
    public String addGameToDeveloper(@PathVariable Long devId, @PathVariable Long gameId) {
        GameDTO gameToAdd = gameService.getById(gameId);

        gameToAdd.getDevelopers().add(developerService.getById(devId));
        gameService.saveGame(gameToAdd);

        return "redirect:/developer/" + devId + "/update";
    }
}

package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.UserDTO;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;


@Slf4j
@Controller
public class UserController {

    private final UserService userService;
    private final GameService gameService;

    private final static String USER_UPDATEFORM = "user/update";
    private final static String USER_CREATEFORM = "user/create";

    public UserController(UserService userService, GameService gameService) {
        this.userService = userService;
        this.gameService = gameService;
    }


    @RequestMapping("/users")
    public String getUsers(Model model) {

        model.addAttribute("users", userService.getSortedUsers());

        return "user";

    }

    @RequestMapping("/user/{id}/view")
    public String getUserById(@PathVariable String id, Model model) {

        model.addAttribute("user", userService.getById(new Long(id)));

        return "user/view";
    }

    @RequestMapping("/user/create")
    public String createUser(Model model) {

        model.addAttribute("user", new UserDTO());

        return USER_CREATEFORM;
    }

    @GetMapping("/user/{id}/delete")
    public String deleteUser(@PathVariable String id) {

        userService.deleteById(Long.valueOf(id));
        return "redirect:/users";

    }

    @GetMapping("user/{id}/update")
    public String updateUser(@PathVariable String id, Model model) {

        UserDTO userDTO = userService.getById(Long.valueOf(id));

        model.addAttribute("user", userDTO);

        Set<GameDTO> displayedGames = gameService.getDisplayedGames(userDTO.getGames());
        SortedSet<GameDTO> sortedDisplayedGames = new TreeSet<>(Comparator.comparing(GameDTO::getId));
        sortedDisplayedGames.addAll(displayedGames);

        model.addAttribute("allGames", sortedDisplayedGames);

        return USER_UPDATEFORM;
    }

    @PostMapping("user/update/submit")
    public String update(@Valid @ModelAttribute("user") UserDTO userDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                log.debug(objectError.toString());
            });
            return USER_UPDATEFORM;
        }

        userDTO.setGames(userService.getById(userDTO.getId()).getGames());

        UserDTO savedUserDTO = userService.saveUser(userDTO);

        return "redirect:/user/" + savedUserDTO.getId() + "/view";

    }

    @PostMapping("user/create/submit")
    public String create(@Valid @ModelAttribute("user") UserDTO userDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> log.debug(objectError.toString()));
            return USER_CREATEFORM;
        }

        UserDTO savedUserDTO = userService.saveUser(userDTO);

        return "redirect:/user/" + savedUserDTO.getId() + "/view";
    }

    @GetMapping("/user/{userId}/game/{gameId}/delete")
    public String deleteGameFromUser(@PathVariable Long userId, @PathVariable Long gameId) {

        UserDTO userDTO = userService.getById(userId);
        Set<GameDTO> gameDTOSet = userDTO.getGames();
        final GameDTO[] gameToDelete = new GameDTO[1];

        gameDTOSet.iterator().forEachRemaining(gameDTO -> {
            if (gameDTO.getId().longValue() == gameId) {
                gameToDelete[0] = gameDTO;
            }
        });

        gameDTOSet.remove(gameToDelete[0]);

        userDTO.setGames(gameDTOSet);
        userService.saveUser(userDTO);

        return "redirect:/user/" + userId + "/update";
    }

    @GetMapping("/user/{userId}/game/{gameId}/add")
    public String addGameToUser(@PathVariable Long userId, @PathVariable Long gameId) {
        GameDTO gameToAdd = gameService.getById(gameId);

        UserDTO userDTO = userService.getById(userId);
        userDTO.getGames().add(gameToAdd);
        userService.saveUser(userDTO);

        return "redirect:/user/" + userId + "/update";
    }
}

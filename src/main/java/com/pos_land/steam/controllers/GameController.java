package com.pos_land.steam.controllers;


import com.pos_land.steam.domain.EGenre;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.services.DeveloperService;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.LanguageService;
import com.pos_land.steam.services.PublisherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Slf4j
@Controller
public class GameController {

    private final GameService gameService;
    private final PublisherService publisherService;
    private final DeveloperService developerService;
    private final LanguageService languageService;

    private final static String GAME_UPDATEFORM = "game/update";
    private final static String GAME_CREATEFORM = "game/create";

    public GameController(GameService gameService, PublisherService publisherService, DeveloperService developerService, LanguageService languageService) {
        this.gameService = gameService;
        this.publisherService = publisherService;
        this.developerService = developerService;
        this.languageService = languageService;
    }

    @RequestMapping("/games")
    public String getGames(Model model) {

        model.addAttribute("games", gameService.getSortedGames());

        return "game";

    }

    @RequestMapping("/game/{id}/view")
    public String getGameById(@PathVariable String id, Model model) {

        model.addAttribute("game", gameService.getById(new Long(id)));

        return "game/view";
    }

    @RequestMapping("/game/create")
    public String createGame(Model model) {

        model.addAttribute("game", new GameDTO());
        model.addAttribute("allGenres", EGenre.getAll());
        model.addAttribute("allPublishers", publisherService.getSortedPublishers());

        return GAME_CREATEFORM;
    }

    @GetMapping("/game/{id}/delete")
    public String deleteGame(@PathVariable String id) {

        gameService.deleteById(Long.valueOf(id));
        return "redirect:/games";

    }

    @GetMapping("/game/{id}/update")
    public String updateGame(@PathVariable String id, Model model) {

        GameDTO gameDTO = gameService.getById(Long.valueOf(id));

        model.addAttribute("game", gameDTO);
        model.addAttribute("allGenres", EGenre.getAll());
        model.addAttribute("allPublishers", publisherService.getSortedPublishers());

        Set<DeveloperDTO> selectedDevelopers = developerService.getSelectedDevelopers(gameDTO.getDevelopers());

        SortedSet<DeveloperDTO> sortedDevelopers = new TreeSet<>(Comparator.comparing(DeveloperDTO::getId));
        sortedDevelopers.addAll(selectedDevelopers);

        model.addAttribute("allDevelopers", sortedDevelopers);

        Set<LanguageDTO> displayLanguages = languageService.getDisplayedLanguages(gameDTO.getLanguages());
        SortedSet<LanguageDTO> sortedLanguages = new TreeSet<>(Comparator.comparing(LanguageDTO::getId));
        sortedLanguages.addAll(displayLanguages);
        model.addAttribute("allLanguages", sortedLanguages);

        return GAME_UPDATEFORM;
    }

    @PostMapping("/game/update/submit")
    public String update(@Valid @ModelAttribute("game") GameDTO gameDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                log.debug(objectError.toString());
            });
            return GAME_UPDATEFORM;
        }

        GameDTO dbGame = gameService.getById(gameDTO.getId());

        gameDTO.setDevelopers(dbGame.getDevelopers());
        gameDTO.setLanguages(dbGame.getLanguages());

        GameDTO savedGameDTO = gameService.saveGame(gameDTO);

        return "redirect:/game/" + savedGameDTO.getId() + "/view";

    }

    @PostMapping("/game/create/submit")
    public String create(@Valid @ModelAttribute("game") GameDTO gameDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> log.debug(objectError.toString()));
            return GAME_CREATEFORM;
        }

        GameDTO savedGameDTO = gameService.saveGame(gameDTO);
        log.error("Genre: " + savedGameDTO.getGenre());

        return "redirect:/game/" + savedGameDTO.getId() + "/view";
    }

    @GetMapping("/game/{gameId}/developer/{devId}/add")
    public String addDeveloperToGame(@PathVariable Long gameId, @PathVariable Long devId) {
        DeveloperDTO developerToAdd = developerService.getById(devId);

        GameDTO gameDTO = gameService.getById(gameId);
        gameDTO.getDevelopers().add(developerToAdd);
        gameService.saveGame(gameDTO);

        return "redirect:/game/" + gameId + "/update";
    }

    @GetMapping("/game/{gameId}/developer/{devId}/delete")
    public String deleteDeveloperFromGame(@PathVariable Long gameId, @PathVariable Long devId) {

        GameDTO gameDTO = gameService.getById(gameId);
        Set<DeveloperDTO> developerDTOSet = gameDTO.getDevelopers();
        final DeveloperDTO[] devToDelete = new DeveloperDTO[1];

        developerDTOSet.iterator().forEachRemaining(developerDTO -> {
            if (developerDTO.getId().longValue() == devId) {
                devToDelete[0] = developerDTO;
            }
        });

        developerDTOSet.remove(devToDelete[0]);

        gameDTO.setDevelopers(developerDTOSet);
        gameService.saveGame(gameDTO);

        return "redirect:/game/" + gameId + "/update";
    }

    @GetMapping("/game/{gameId}/language/{langId}/add")
    public String addLanguageToGame(@PathVariable Long gameId, @PathVariable Long langId) {
        LanguageDTO languageToAdd = languageService.getById(langId);

        GameDTO gameDTO = gameService.getById(gameId);
        gameDTO.getLanguages().add(languageToAdd);
        gameService.saveGame(gameDTO);

        return "redirect:/game/" + gameId + "/update";
    }

    @GetMapping("/game/{gameId}/language/{langId}/delete")
    public String deleteLanguageFromGame(@PathVariable Long gameId, @PathVariable Long langId) {

        GameDTO gameDTO = gameService.getById(gameId);
        Set<LanguageDTO> languageDTOSet = gameDTO.getLanguages();
        final LanguageDTO[] langToDelete = new LanguageDTO[1];

        languageDTOSet.iterator().forEachRemaining(languageDTO -> {
            if (languageDTO.getId().longValue() == langId) {
                langToDelete[0] = languageDTO;
            }
        });

        languageDTOSet.remove(langToDelete[0]);

        gameDTO.setLanguages(languageDTOSet);
        gameService.saveGame(gameDTO);

        return "redirect:/game/" + gameId + "/update";
    }

}

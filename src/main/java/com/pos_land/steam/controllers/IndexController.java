package com.pos_land.steam.controllers;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
public class IndexController {

//    private final UserService userService;
//    private final DeveloperService developerService;
//    private final GameService gameService;
//
//    public IndexController(UserService userService, DeveloperService developerService, GameService gameService) {
//        this.userService = userService;
//        this.developerService = developerService;
//        this.gameService = gameService;
//    }

    @RequestMapping({"/", ""})
    public String getStartPage(Model model) {

//        model.addAttribute("users", userService.getGames());
//        model.addAttribute("developers", developerService.getDevelopers());
//        model.addAttribute("games", gameService.getGames());

        return "index";
    }

}

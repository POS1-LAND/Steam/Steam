package com.pos_land.steam.controllers;

import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.PublisherDTO;
import com.pos_land.steam.services.GameService;
import com.pos_land.steam.services.PublisherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

@Slf4j
@Controller
public class PublisherController {

    private final PublisherService publisherService;
    private final GameService gameService;

    public PublisherController(PublisherService publisherService, GameService gameService) {
        this.publisherService = publisherService;
        this.gameService = gameService;
    }

    @RequestMapping({"/publishers"})
    public String getPublishers(Model model){
        model.addAttribute("publishers", publisherService.getSortedPublishers());
        return "publisher";
    }

    @RequestMapping("/publisher/{id}/view")
    public String getPublisherById(@PathVariable String id, Model model){
        model.addAttribute("publisher", publisherService.getById(Long.valueOf(id)));
        return "publisher/view";
    }

    @RequestMapping("/publisher/create")
    public String createPublisher(Model model){
        model.addAttribute("publisher", new PublisherDTO());
        return "publisher/create";
    }

    @GetMapping("/publisher/{id}/delete")
    public String deletePublisher(@PathVariable Long id){
        publisherService.deleteById(Long.valueOf(id));
        return "redirect:/publishers";
    }

    @GetMapping("/publisher/{id}/update")
    public String updatePublisher(@PathVariable String id, Model model){
        model.addAttribute("publisher", publisherService.getById(Long.valueOf(id)));

        // allGames attribute for a drop-down list of all existing games to easily add games to publishers
        SortedSet<GameDTO> gameDTOSet = new TreeSet<>(Comparator.comparing(GameDTO::getId));
        gameService.getSortedGames().forEach(gameDTO -> {
            if(gameDTO.getPublisher() == null){
                gameDTOSet.add(gameDTO);
            }
        });
        model.addAttribute("allGames", gameDTOSet);

        return "publisher/update";
    }

    @PostMapping("publisher/update/submit")
    public String update(@Valid @ModelAttribute("publisher") PublisherDTO publisherDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> {
                log.debug(objectError.toString());
            });
            return "publisher/update";
        }

        PublisherDTO savedPublisherDTO = publisherService.savePublisher(publisherDTO);

        return "redirect:/publisher/" + savedPublisherDTO.getId() + "/view";

    }

    @PostMapping("publisher/create/submit")
    public String create(@Valid @ModelAttribute("publisher") PublisherDTO publisherDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(objectError -> log.debug(objectError.toString()));
            return "publisher/create";
        }

        PublisherDTO savedPublisherDTO = publisherService.savePublisher(publisherDTO);

        return "redirect:/publisher/" + savedPublisherDTO.getId() + "/view";
    }

    @GetMapping("/publisher/{pubId}/game/{gameId}/delete")
    public String deleteGameFromPublisher(@PathVariable Long pubId, @PathVariable Long gameId) {
        GameDTO gameToDelete = gameService.getById(gameId);
        gameToDelete.setPublisher(null);
        gameService.saveGame(gameToDelete);

        return "redirect:/publisher/" + pubId + "/update";
    }

    @GetMapping("/publisher/{pubId}/game/{gameId}/add")
    public String addGameToPublisher(@PathVariable Long pubId, @PathVariable Long gameId) {
        GameDTO gameToAdd = gameService.getById(gameId);

        if (gameToAdd.getPublisher() == null) {
            gameToAdd.setPublisher(publisherService.getById(pubId));
            gameService.saveGame(gameToAdd);
        } else {
            log.debug("Game with ID " + gameId + " already has a publisher.");
        }

        return "redirect:/publisher/" + pubId + "/update";
    }
}

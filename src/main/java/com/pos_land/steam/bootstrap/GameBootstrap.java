package com.pos_land.steam.bootstrap;

import com.pos_land.steam.converters.GameConverter;
import com.pos_land.steam.domain.EGenre;
import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.dto.GameDTO;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.repositories.GameRepository;
import com.pos_land.steam.services.DeveloperService;
import com.pos_land.steam.services.LanguageService;
import com.pos_land.steam.services.PublisherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
@DependsOn(value = {"languageBootstrap", "developerBootstrap", "publisherBootstrap"})
public class GameBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final GameRepository gameRepository;
    private final GameConverter gameConverter;
    private final DeveloperService developerService;
    private final LanguageService languageService;
    private final PublisherService publisherService;

    public GameBootstrap(GameRepository gameRepository, GameConverter gameConverter, DeveloperService developerService, LanguageService languageService, PublisherService publisherService) {
        this.gameRepository = gameRepository;
        this.gameConverter = gameConverter;
        this.developerService = developerService;
        this.languageService = languageService;
        this.publisherService = publisherService;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.debug("Loading Game Bootstrap data");
        gameRepository.saveAll(getGames());
    }

    public List<Game> getGames() {
        List<Game> games = new ArrayList<>(4);

        GameDTO commandConquerGame = new GameDTO();
        commandConquerGame.setId(1L);
        commandConquerGame.setDescription("A realtime Strategy Game");
        commandConquerGame.setGameName("Command and Conquer 3");
        commandConquerGame.setGenre(EGenre.STRATEGY);
        Set<LanguageDTO> languages = languageService.getLanguages();
        commandConquerGame.setLanguages(languages);
        commandConquerGame.setPrice(29.99);
        commandConquerGame.setPublisher(null);
        Set<DeveloperDTO> developers = developerService.getSortedDevelopers();
        commandConquerGame.setDevelopers(developers);
        commandConquerGame.setReleaseDate(new Date());

        games.add(gameConverter.convert(commandConquerGame));


        GameDTO gothicGame = new GameDTO();
        gothicGame.setId(2L);
        gothicGame.setDescription("A medieval RPG");
        gothicGame.setGameName("Gothic");
        gothicGame.setGenre(EGenre.RPG);
        gothicGame.setLanguages(languages);
        gothicGame.setPrice(9.99);
        gothicGame.setDevelopers(developers);
        gothicGame.setPublisher(publisherService.getById(1L));
        gothicGame.setReleaseDate(new Date());
        games.add(gameConverter.convert(gothicGame));


        GameDTO darkSoulsGame = new GameDTO();
        darkSoulsGame.setId(3L);
        darkSoulsGame.setDescription("A challenging dark RPG game with dozens of bosses");
        darkSoulsGame.setGameName("Dark Souls III");
        darkSoulsGame.setGenre(EGenre.RPG);
        darkSoulsGame.setLanguages(languages);
        darkSoulsGame.setPrice(59.99);
        darkSoulsGame.setPublisher(publisherService.getById(2L));
        darkSoulsGame.setDevelopers(developers);

        darkSoulsGame.setReleaseDate(new Date());

        games.add(gameConverter.convert(darkSoulsGame));

        return games;
    }
}

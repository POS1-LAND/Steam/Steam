package com.pos_land.steam.bootstrap;

import com.pos_land.steam.converters.UserConverter;
import com.pos_land.steam.domain.User;
import com.pos_land.steam.dto.UserDTO;
import com.pos_land.steam.repositories.UserRepository;
import com.pos_land.steam.services.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@DependsOn(value = "gameBootstrap")
public class UserBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;
    private final UserConverter userConverter;
    private final GameService gameService;

    public UserBootstrap(UserRepository userRepository, UserConverter userConverter, GameService gameService) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
        this.gameService = gameService;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.debug("Loading User Bootstrap data");
        userRepository.saveAll(getUsers());
    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>(2);

        UserDTO andiUser = new UserDTO();
        andiUser.setId(1L);
        andiUser.setEmail("andimurk@hotmail.de");
        andiUser.setUserName("AndiM202");
        andiUser.setPassword("123456");
        andiUser.getGames().add(gameService.getById(1L));
        andiUser.getGames().add(gameService.getById(2L));
        andiUser.getGames().add(gameService.getById(3L));

        users.add(userConverter.convert(andiUser));

        UserDTO gredlerUser = new UserDTO();
        gredlerUser.setId(2L);
        gredlerUser.setEmail("angredler@tsn.at");
        gredlerUser.setUserName("Volecheck");
        gredlerUser.setPassword("12345678");
        users.add(userConverter.convert(gredlerUser));

        UserDTO lolleUser = new UserDTO();
        lolleUser.setId(3L);
        lolleUser.setEmail("lbroger@tsn.at");
        lolleUser.setUserName("Lolle");
        lolleUser.setPassword("12345678");

        users.add(userConverter.convert(lolleUser));

        return users;
    }
}

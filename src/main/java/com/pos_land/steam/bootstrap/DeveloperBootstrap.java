package com.pos_land.steam.bootstrap;

import com.pos_land.steam.converters.DeveloperConverter;
import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.dto.DeveloperDTO;
import com.pos_land.steam.repositories.DeveloperRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class DeveloperBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final DeveloperRepository developerRepository;
    private final DeveloperConverter developerConverter;

    public DeveloperBootstrap(DeveloperRepository developerRepository, DeveloperConverter developerConverter) {
        this.developerRepository = developerRepository;
        this.developerConverter = developerConverter;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.debug("Loading Developer Bootstrap data");
        developerRepository.saveAll(getDevelopers());
    }

    private List<Developer> getDevelopers() {

        List<Developer> developers = new ArrayList<>(2);

        DeveloperDTO electronicDev = new DeveloperDTO();
        electronicDev.setDeveloperName("Electronic Arts");
        electronicDev.setId(1L);

        developers.add(developerConverter.convert(electronicDev));

        DeveloperDTO jowoodDev = new DeveloperDTO();
        jowoodDev.setDeveloperName("Jowood");
        jowoodDev.setId(2L);

        developers.add(developerConverter.convert(jowoodDev));

        return developers;
    }
}

package com.pos_land.steam.bootstrap;

import com.pos_land.steam.converters.PublisherConverter;
import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.PublisherDTO;
import com.pos_land.steam.repositories.PublisherRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class PublisherBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private PublisherRepository publisherRepository;
    private PublisherConverter publisherConverter;

    public PublisherBootstrap(PublisherRepository publisherRepository, PublisherConverter publisherConverter) {
        this.publisherRepository = publisherRepository;
        this.publisherConverter = publisherConverter;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.debug("Loading Publisher Bootstrap Data");
        publisherRepository.saveAll(getPublishers());
    }

    public List<Publisher> getPublishers(){
        List<Publisher> publisherSet = new ArrayList<>();

        PublisherDTO publisher1 = new PublisherDTO();
        publisher1.setId(1L);
        publisher1.setPublisherName("BANDAI NAMCO");
        publisherSet.add(publisherConverter.convert(publisher1));

        PublisherDTO publisher2 = new PublisherDTO();
        publisher2.setId(2L);
        publisher2.setPublisherName("Blue Byte");
        publisherSet.add(publisherConverter.convert(publisher2));

        PublisherDTO publisher3 = new PublisherDTO();
        publisher3.setId(3L);
        publisher3.setPublisherName("Blizzard");
        publisherSet.add(publisherConverter.convert(publisher3));

        PublisherDTO publisher4 = new PublisherDTO();
        publisher4.setId(4L);
        publisher4.setPublisherName("Daybreak Game Company");
        publisherSet.add(publisherConverter.convert(publisher4));

        PublisherDTO publisher5 = new PublisherDTO();
        publisher5.setId(5L);
        publisher5.setPublisherName("Cloud Imperium Games");
        publisherSet.add(publisherConverter.convert(publisher5));

        PublisherDTO publisher6 = new PublisherDTO();
        publisher6.setId(6L);
        publisher6.setPublisherName("Bluehole Studio");
        publisherSet.add(publisherConverter.convert(publisher6));

        return publisherSet;

    }
}

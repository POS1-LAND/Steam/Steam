package com.pos_land.steam.bootstrap;

import com.pos_land.steam.converters.LanguageConverter;
import com.pos_land.steam.domain.ELanguage;
import com.pos_land.steam.domain.Language;
import com.pos_land.steam.dto.LanguageDTO;
import com.pos_land.steam.repositories.LanguageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class LanguageBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final LanguageRepository languageRepository;
    private final LanguageConverter languageConverter;

    public LanguageBootstrap(LanguageRepository languageRepository, LanguageConverter languageConverter) {
        this.languageRepository = languageRepository;
        this.languageConverter = languageConverter;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.debug("Loading Language Bootstrap Data");
        languageRepository.saveAll(getLanguages());
    }

    private List<Language> getLanguages() {
        List<Language> languages = new ArrayList<>();

        LanguageDTO engLanguage = new LanguageDTO();
        engLanguage.setId(1L);
        engLanguage.setLanguage(ELanguage.ENGLISH);

        LanguageDTO gerLanguage = new LanguageDTO();
        gerLanguage.setId(2L);
        gerLanguage.setLanguage(ELanguage.GERMAN);

        LanguageDTO spanLanguage = new LanguageDTO();
        spanLanguage.setId(3L);
        spanLanguage.setLanguage(ELanguage.SPANISH);


        languages.add(languageConverter.convert(engLanguage));
        languages.add(languageConverter.convert(gerLanguage));
        languages.add(languageConverter.convert(spanLanguage));

        return languages;
    }
}

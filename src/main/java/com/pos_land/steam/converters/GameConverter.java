package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.GameDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class GameConverter implements Converter<GameDTO, Game> {

    private final SimpleDeveloperConverter developerConverter;
    private final SimplePublisherConverter publisherConverter;
    private final LanguageConverter languageConverter;

    public GameConverter(SimpleDeveloperConverter developerConverter, SimplePublisherConverter publisherConverter, LanguageConverter languageConverter) {
        this.developerConverter = developerConverter;
        this.publisherConverter = publisherConverter;
        this.languageConverter = languageConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public Game convert(GameDTO source) {
        if (source == null) {
            return null;
        }

        final Game game = new Game();
        game.setId(source.getId());
        game.setDescription(source.getDescription());
        game.setGameName(source.getGameName());
        game.setPrice(source.getPrice());
        game.setReleaseDate(source.getReleaseDate());
        game.setGenre(source.getGenre());
        game.setPublisher(publisherConverter.convert(source.getPublisher()));


        if (source.getDevelopers() != null && source.getDevelopers().size() > 0) {
            source.getDevelopers()
                    .forEach(developerDTO -> game.getDevelopers().add(developerConverter.convert(developerDTO)));
        }
        if (source.getLanguages() != null && source.getLanguages().size() > 0) {
            source.getLanguages()
                    .forEach(languageDTO -> game.getLanguages().add(languageConverter.convert(languageDTO)));
        }

        return game;

    }

    @Synchronized
    @Nullable
    public GameDTO convert(Game source) {
        if (source == null) {
            return null;
        }
        final GameDTO gameDTO = new GameDTO();
        gameDTO.setId(source.getId());
        gameDTO.setDescription(source.getDescription());
        gameDTO.setGameName(source.getGameName());
        gameDTO.setPrice(source.getPrice());
        gameDTO.setReleaseDate(source.getReleaseDate());
        gameDTO.setGenre(source.getGenre());
        gameDTO.setPublisher(publisherConverter.convert(source.getPublisher()));


        if (source.getDevelopers() != null && source.getDevelopers().size() > 0) {
            source.getDevelopers()
                    .forEach(developer -> gameDTO.getDevelopers().add(developerConverter.convert(developer)));
        }
        if (source.getLanguages() != null && source.getLanguages().size() > 0) {
            source.getLanguages()
                    .forEach(language -> gameDTO.getLanguages().add(languageConverter.convert(language)));
        }

        return gameDTO;
    }
}

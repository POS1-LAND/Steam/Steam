package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Language;
import com.pos_land.steam.dto.LanguageDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class LanguageConverter implements Converter<LanguageDTO, Language> {

    @Synchronized
    @Nullable
    @Override
    public Language convert(LanguageDTO source) {
        if (source == null) {
            return null;
        }

        final Language language = new Language();
        language.setId(source.getId());
        language.setLanguage(source.getLanguage());

        return language;
    }

    @Synchronized
    @Nullable
    public LanguageDTO convert(Language source) {
        if (source == null) {
            return null;
        }

        final LanguageDTO languageDTO = new LanguageDTO();
        languageDTO.setId(source.getId());
        languageDTO.setLanguage(source.getLanguage());

        return languageDTO;
    }

}

package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.dto.DeveloperDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class DeveloperConverter implements Converter<DeveloperDTO, Developer> {

    private final SimpleGameConverter gameConverter;

    public DeveloperConverter(SimpleGameConverter gameConverter) {
        this.gameConverter = gameConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public Developer convert(DeveloperDTO source) {
        if (source == null) {
            return null;
        }
        final Developer developer = new Developer();
        developer.setId(source.getId());
        developer.setDeveloperName(source.getDeveloperName());

        if(source.getGames() != null && source.getGames().size() > 0){
            source.getGames()
                    .forEach(gameDTO -> developer.getGames().add(gameConverter.convert(gameDTO)));
        }

        return developer;
    }

    @Synchronized
    @Nullable
    public DeveloperDTO convert(Developer source) {
        if (source == null) {
            return null;
        }
        final DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(source.getId());
        developerDTO.setDeveloperName(source.getDeveloperName());

        if(source.getGames() != null && source.getGames().size() > 0 ){
            source.getGames()
                    .forEach(game -> developerDTO.getGames().add(gameConverter.convert(game)));
        }

        return developerDTO;
    }
}

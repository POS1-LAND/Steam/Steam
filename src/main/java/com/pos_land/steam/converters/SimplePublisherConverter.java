package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.PublisherDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class SimplePublisherConverter implements Converter<PublisherDTO, Publisher> {

    @Synchronized
    @Nullable
    @Override
    public Publisher convert(PublisherDTO source) {
        if(source == null){
            return null;
        }
        Publisher publisher = new Publisher();
        publisher.setId(source.getId());
        publisher.setPublisherName(source.getPublisherName());
        return publisher;
    }

    @Synchronized
    @Nullable
    public PublisherDTO convert(Publisher source){
        if(source == null){
            return null;
        }
        PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(source.getId());
        publisherDTO.setPublisherName(source.getPublisherName());
        return publisherDTO;
    }
}

package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Developer;
import com.pos_land.steam.dto.DeveloperDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class SimpleDeveloperConverter implements Converter<Developer, DeveloperDTO> {

    @Synchronized
    @Nullable
    @Override
    public DeveloperDTO convert(Developer source) {
        if(source == null){
            return null;
        }

        DeveloperDTO developerDTO = new DeveloperDTO();
        developerDTO.setId(source.getId());
        developerDTO.setDeveloperName(source.getDeveloperName());
        return developerDTO;
    }

    @Synchronized
    @Nullable
    public Developer convert(DeveloperDTO source){
        if(source == null){
            return null;
        }

        Developer developer = new Developer();
        developer.setId(source.getId());
        developer.setDeveloperName(source.getDeveloperName());
        return developer;
    }
}

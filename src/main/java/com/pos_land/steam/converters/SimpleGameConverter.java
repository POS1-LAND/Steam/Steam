package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Game;
import com.pos_land.steam.dto.GameDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class SimpleGameConverter implements Converter<GameDTO, Game> {


    @Synchronized
    @Nullable
    @Override
    public Game convert(GameDTO source) {
        if (source == null) {
            return null;
        }

        final Game game = new Game();
        game.setId(source.getId());
        game.setDescription(source.getDescription());
        game.setGameName(source.getGameName());
        game.setPrice(source.getPrice());
        game.setReleaseDate(source.getReleaseDate());
        game.setGenre(source.getGenre());

        return game;
    }

    @Synchronized
    @Nullable
    public GameDTO convert(Game source) {
        if (source == null) {
            return null;
        }

        final GameDTO gameDTO = new GameDTO();
        gameDTO.setId(source.getId());
        gameDTO.setDescription(source.getDescription());
        gameDTO.setGameName(source.getGameName());
        gameDTO.setPrice(source.getPrice());
        gameDTO.setReleaseDate(source.getReleaseDate());
        gameDTO.setGenre(source.getGenre());

        return gameDTO;
    }
}

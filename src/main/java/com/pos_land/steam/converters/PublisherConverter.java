package com.pos_land.steam.converters;

import com.pos_land.steam.domain.Publisher;
import com.pos_land.steam.dto.PublisherDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class PublisherConverter implements Converter<PublisherDTO, Publisher> {

    private final SimpleGameConverter gameConverter;

    public PublisherConverter(SimpleGameConverter gameConverter) {
        this.gameConverter = gameConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public Publisher convert(PublisherDTO source) {
        if (source == null) {
            return null;
        }

        final Publisher publisher = new Publisher();
        publisher.setId(source.getId());
        publisher.setPublisherName(source.getPublisherName());

            if (source.getGames() != null && source.getGames().size() > 0) {
                source.getGames()
                        .forEach(gameDTO -> publisher.getGames().add(gameConverter.convert(gameDTO)));
            }

        return publisher;
    }

    @Synchronized
    @Nullable
    public PublisherDTO convert(Publisher source) {
        if (source == null) {
            return null;
        }

        final PublisherDTO publisherDTO = new PublisherDTO();
        publisherDTO.setId(source.getId());
        publisherDTO.setPublisherName(source.getPublisherName());

        if (source.getGames() != null && source.getGames().size() > 0) {
            source.getGames()
                    .forEach(game -> publisherDTO.getGames().add(gameConverter.convert(game)));
        }

        return publisherDTO;
    }
}

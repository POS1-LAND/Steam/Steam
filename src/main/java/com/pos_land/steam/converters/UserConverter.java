package com.pos_land.steam.converters;

import com.pos_land.steam.domain.User;
import com.pos_land.steam.dto.UserDTO;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserConverter implements Converter<UserDTO, User> {

    private final GameConverter gameConverter;

    public UserConverter(GameConverter gameConverter) {
        this.gameConverter = gameConverter;
    }

    @Synchronized
    @Nullable
    @Override
    public User convert(UserDTO source) {
        if (source == null) {
            return null;
        }
        final User user = new User();
        user.setId(source.getId());
        user.setEmail(source.getEmail());
        user.setPassword(source.getPassword());
        user.setUserName(source.getUserName());

        if (source.getGames() != null && source.getGames().size() > 0) {
            source.getGames()
                    .forEach(gameDTO -> user.getGames().add(gameConverter.convert(gameDTO)));
        }

        return user;
    }

    @Synchronized
    @Nullable
    public UserDTO convert(User source) {
        if (source == null) {
            return null;
        }
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(source.getId());
        userDTO.setEmail(source.getEmail());
        userDTO.setPassword(source.getPassword());
        userDTO.setUserName(source.getUserName());

        if (source.getGames() != null && source.getGames().size() > 0) {
            source.getGames()
                    .forEach(game -> userDTO.getGames().add(gameConverter.convert(game)));
         }

        return userDTO;
    }
}

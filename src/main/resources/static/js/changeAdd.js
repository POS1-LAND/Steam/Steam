function changeAddLink(selId, buttonId, addItem) {
    var sel = document.getElementById(selId);
    var gameId = sel.options[sel.selectedIndex].value;
    var button = document.getElementById(buttonId);

    button.href = addItem + "/" + gameId + "/add";
}

function hrefForDeletingElementAttribute(element, elementId,  attribute) {
    var output = document.getElementById("output");
    output.setAttribute("href", "/" + element + "/" + elementId + '/game/' + attribute + "/delete");
    document.getElementById('modal').style.display='block';
}

function hrefForDeletingElement(element, elementId) {
    var output = document.getElementById("output");
    output.setAttribute("href", "/" + element + "/" + elementId + "/delete");
    document.getElementById('modal').style.display='block';
}